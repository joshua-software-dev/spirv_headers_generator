const std = @import("std");

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    const spirv_headers_dep = b.dependency("spirv_headers", .{});
    const spirv_tools_dep = b.dependency("spirv_tools", .{});

    const generator_dep = b.dependency("generator", .{
        .target = target,
        .optimize = optimize,
        .spirv_headers_path = spirv_headers_dep.path("").getPath(b),
        .spirv_tools_path = spirv_tools_dep.path("").getPath(b),
    });
    const spirv_headers_generated = generator_dep.namedWriteFiles("spirv_headers_generated");

    const install_step = b.getInstallStep();
    var zig_gen_file_map = std.StringArrayHashMap(*std.Build.Step.WriteFile.File).init(b.allocator);
    defer zig_gen_file_map.deinit();
    for (spirv_headers_generated.files.items) |file| {
        try zig_gen_file_map.put(file.sub_path, file);
        install_step.dependOn(
            &b.addInstallFileWithDir(
                file.getPath(),
                .header,
                b.fmt("zig/{s}", .{ file.sub_path }),
            ).step
        );
    }

    const python_path = try b.findProgram(&.{ "python3", "python" }, &.{});
    const sha256_compare = b.addExecutable(.{
        .name = "sha256_compare",
        .root_source_file = b.path("src/sha256_compare.zig"),
        .target = b.host,
        .optimize = optimize,
    });

    const run_compare1 = b.addRunArtifact(sha256_compare);
    run_compare1.stdio = .inherit;
    run_compare1.addFileArg(zig_gen_file_map.get("build-version.inc").?.getPath());

    const run_compare2 = b.addRunArtifact(sha256_compare);
    run_compare2.stdio = .inherit;
    run_compare2.addFileArg(zig_gen_file_map.get("generators.inc").?.getPath());
    run_compare2.step.dependOn(&run_compare1.step);

    const run_compare3 = b.addRunArtifact(sha256_compare);
    run_compare3.stdio = .inherit;
    run_compare3.addFileArg(zig_gen_file_map.get("DebugInfo.h").?.getPath());
    run_compare3.step.dependOn(&run_compare2.step);

    const run_compare4 = b.addRunArtifact(sha256_compare);
    run_compare4.stdio = .inherit;
    run_compare4.addFileArg(zig_gen_file_map.get("NonSemanticShaderDebugInfo100.h").?.getPath());
    run_compare4.step.dependOn(&run_compare3.step);

    const run_compare5 = b.addRunArtifact(sha256_compare);
    run_compare5.stdio = .inherit;
    run_compare5.addFileArg(zig_gen_file_map.get("OpenCLDebugInfo100.h").?.getPath());
    run_compare5.step.dependOn(&run_compare4.step);

    const run_compare6 = b.addRunArtifact(sha256_compare);
    run_compare6.stdio = .inherit;
    run_compare6.addFileArg(zig_gen_file_map.get("spv-amd-shader-explicit-vertex-parameter.insts.inc").?.getPath());
    run_compare6.step.dependOn(&run_compare5.step);

    const run_compare7 = b.addRunArtifact(sha256_compare);
    run_compare7.stdio = .inherit;
    run_compare7.addFileArg(zig_gen_file_map.get("spv-amd-shader-trinary-minmax.insts.inc").?.getPath());
    run_compare7.step.dependOn(&run_compare6.step);

    const run_compare8 = b.addRunArtifact(sha256_compare);
    run_compare8.stdio = .inherit;
    run_compare8.addFileArg(zig_gen_file_map.get("spv-amd-gcn-shader.insts.inc").?.getPath());
    run_compare8.step.dependOn(&run_compare7.step);

    const run_compare9 = b.addRunArtifact(sha256_compare);
    run_compare9.stdio = .inherit;
    run_compare9.addFileArg(zig_gen_file_map.get("spv-amd-shader-ballot.insts.inc").?.getPath());
    run_compare9.step.dependOn(&run_compare8.step);

    const run_compare10 = b.addRunArtifact(sha256_compare);
    run_compare10.stdio = .inherit;
    run_compare10.addFileArg(zig_gen_file_map.get("debuginfo.insts.inc").?.getPath());
    run_compare10.step.dependOn(&run_compare9.step);

    const run_compare11 = b.addRunArtifact(sha256_compare);
    run_compare11.stdio = .inherit;
    run_compare11.addFileArg(zig_gen_file_map.get("opencl.debuginfo.100.insts.inc").?.getPath());
    run_compare11.step.dependOn(&run_compare10.step);

    const run_compare12 = b.addRunArtifact(sha256_compare);
    run_compare12.stdio = .inherit;
    run_compare12.addFileArg(zig_gen_file_map.get("nonsemantic.shader.debuginfo.100.insts.inc").?.getPath());
    run_compare12.step.dependOn(&run_compare11.step);

    const run_compare13 = b.addRunArtifact(sha256_compare);
    run_compare13.stdio = .inherit;
    run_compare13.addFileArg(zig_gen_file_map.get("nonsemantic.clspvreflection.insts.inc").?.getPath());
    run_compare13.step.dependOn(&run_compare12.step);

    const run_compare14 = b.addRunArtifact(sha256_compare);
    run_compare14.stdio = .inherit;
    run_compare14.addFileArg(zig_gen_file_map.get("core.insts-unified1.inc").?.getPath());
    run_compare14.step.dependOn(&run_compare13.step);

    const run_compare15 = b.addRunArtifact(sha256_compare);
    run_compare15.stdio = .inherit;
    run_compare15.addFileArg(zig_gen_file_map.get("operand.kinds-unified1.inc").?.getPath());
    run_compare15.step.dependOn(&run_compare14.step);

    const run_compare16 = b.addRunArtifact(sha256_compare);
    run_compare16.stdio = .inherit;
    run_compare16.addFileArg(zig_gen_file_map.get("extension_enum.inc").?.getPath());
    run_compare16.step.dependOn(&run_compare15.step);

    const run_compare17 = b.addRunArtifact(sha256_compare);
    run_compare17.stdio = .inherit;
    run_compare17.addFileArg(zig_gen_file_map.get("enum_string_mapping.inc").?.getPath());
    run_compare17.step.dependOn(&run_compare16.step);

    const run_compare18 = b.addRunArtifact(sha256_compare);
    run_compare18.stdio = .inherit;
    run_compare18.addFileArg(zig_gen_file_map.get("glsl.std.450.insts.inc").?.getPath());
    run_compare18.step.dependOn(&run_compare17.step);

    const run_compare19 = b.addRunArtifact(sha256_compare);
    run_compare19.stdio = .inherit;
    run_compare19.addFileArg(zig_gen_file_map.get("opencl.std.insts.inc").?.getPath());
    run_compare19.step.dependOn(&run_compare18.step);

    const run_compare20 = b.addRunArtifact(sha256_compare);
    run_compare20.stdio = .inherit;
    run_compare20.addFileArg(zig_gen_file_map.get("nonsemantic.vkspreflection.insts.inc").?.getPath());
    run_compare20.step.dependOn(&run_compare19.step);

    install_step.dependOn(&run_compare20.step);

    {
        const run_update_build_ver = b.addSystemCommand(&.{ python_path });
        run_update_build_ver.addFileArg(spirv_tools_dep.path("utils/update_build_version.py"));
        run_update_build_ver.addFileArg(spirv_tools_dep.path("CHANGES"));
        const @"build-version.inc" = run_update_build_ver.addOutputFileArg("build-version.inc");
        run_update_build_ver.setEnvironmentVariable("SOURCE_DATE_EPOCH", "0");

        run_compare1.addFileArg(@"build-version.inc");

        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"build-version.inc",
                .header,
                "python/build-version.inc",
            ).step
        );
    }

    {
        const run_gen_reg_tables = b.addSystemCommand(&.{ python_path });
        run_gen_reg_tables.addFileArg(spirv_tools_dep.path("utils/generate_registry_tables.py"));
        run_gen_reg_tables.addPrefixedFileArg("--xml=", spirv_headers_dep.path("include/spirv/spir-v.xml"));
        const @"generators.inc" = run_gen_reg_tables.addPrefixedOutputFileArg("--generator-output=", "generators.inc");

        run_compare2.addFileArg(@"generators.inc");

        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"generators.inc",
                .header,
                "python/generators.inc",
            ).step
        );
    }

    {
        const script_lazypath = spirv_tools_dep.path("utils/generate_language_headers.py");

        const run_gen_lang_headers1 = b.addSystemCommand(&.{ python_path });
        run_gen_lang_headers1.addFileArg(script_lazypath);
        run_gen_lang_headers1.addPrefixedFileArg(
            "--extinst-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/extinst.debuginfo.grammar.json"),
        );
        const @"DebugInfo.h" = run_gen_lang_headers1.addPrefixedOutputFileArg(
            "--extinst-output-path=",
            "DebugInfo.h",
        );

        const run_gen_lang_headers2 = b.addSystemCommand(&.{ python_path });
        run_gen_lang_headers2.addFileArg(script_lazypath);
        run_gen_lang_headers2.addPrefixedFileArg(
            "--extinst-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/extinst.nonsemantic.shader.debuginfo.100.grammar.json"),
        );
        const @"NonSemanticShaderDebugInfo100.h" = run_gen_lang_headers2.addPrefixedOutputFileArg(
            "--extinst-output-path=",
            "NonSemanticShaderDebugInfo100.h",
        );

        const run_gen_lang_headers3 = b.addSystemCommand(&.{ python_path });
        run_gen_lang_headers3.addFileArg(script_lazypath);
        run_gen_lang_headers3.addPrefixedFileArg(
            "--extinst-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/extinst.opencl.debuginfo.100.grammar.json"),
        );
        const @"OpenCLDebugInfo100.h" = run_gen_lang_headers3.addPrefixedOutputFileArg(
            "--extinst-output-path=",
            "OpenCLDebugInfo100.h",
        );

        run_compare3.addFileArg(@"DebugInfo.h");
        run_compare4.addFileArg(@"NonSemanticShaderDebugInfo100.h");
        run_compare5.addFileArg(@"OpenCLDebugInfo100.h");

        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"DebugInfo.h",
                .header,
                "python/DebugInfo.h",
            ).step
        );
        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"NonSemanticShaderDebugInfo100.h",
                .header,
                "python/NonSemanticShaderDebugInfo100.h",
            ).step
        );
        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"OpenCLDebugInfo100.h",
                .header,
                "python/OpenCLDebugInfo100.h",
            ).step
        );
    }

    {
        const script_lazypath = spirv_tools_dep.path("utils/generate_grammar_tables.py");

        const run_gen_grammar_tables1 = b.addSystemCommand(&.{ python_path });
        run_gen_grammar_tables1.addFileArg(script_lazypath);
        run_gen_grammar_tables1.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            spirv_headers_dep.path(
                "include/spirv/unified1/extinst.spv-amd-shader-explicit-vertex-parameter.grammar.json",
            ),
        );
        const @"spv-amd-shader-explicit-vertex-parameter.insts.inc" = run_gen_grammar_tables1.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "spv-amd-shader-explicit-vertex-parameter.insts.inc",
        );
        run_gen_grammar_tables1.addArg("--vendor-operand-kind-prefix=");

        const run_gen_grammar_tables2 = b.addSystemCommand(&.{ python_path });
        run_gen_grammar_tables2.addFileArg(script_lazypath);
        run_gen_grammar_tables2.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/extinst.spv-amd-shader-trinary-minmax.grammar.json"),
        );
        const @"spv-amd-shader-trinary-minmax.insts.inc" = run_gen_grammar_tables2.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "spv-amd-shader-trinary-minmax.insts.inc",
        );
        run_gen_grammar_tables2.addArg("--vendor-operand-kind-prefix=");

        const run_gen_grammar_tables3 = b.addSystemCommand(&.{ python_path });
        run_gen_grammar_tables3.addFileArg(script_lazypath);
        run_gen_grammar_tables3.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/extinst.spv-amd-gcn-shader.grammar.json"),
        );
        const @"spv-amd-gcn-shader.insts.inc" = run_gen_grammar_tables3.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "spv-amd-gcn-shader.insts.inc",
        );
        run_gen_grammar_tables3.addArg("--vendor-operand-kind-prefix=");

        const run_gen_grammar_tables4 = b.addSystemCommand(&.{ python_path });
        run_gen_grammar_tables4.addFileArg(script_lazypath);
        run_gen_grammar_tables4.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/extinst.spv-amd-shader-ballot.grammar.json"),
        );
        const @"spv-amd-shader-ballot.insts.inc" = run_gen_grammar_tables4.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "spv-amd-shader-ballot.insts.inc",
        );
        run_gen_grammar_tables4.addArg("--vendor-operand-kind-prefix=");

        const run_gen_grammar_tables5 = b.addSystemCommand(&.{ python_path });
        run_gen_grammar_tables5.addFileArg(script_lazypath);
        run_gen_grammar_tables5.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/extinst.debuginfo.grammar.json"),
        );
        const @"debuginfo.insts.inc" = run_gen_grammar_tables5.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "debuginfo.insts.inc",
        );
        run_gen_grammar_tables5.addArg("--vendor-operand-kind-prefix=");

        const run_gen_grammar_tables6 = b.addSystemCommand(&.{ python_path });
        run_gen_grammar_tables6.addFileArg(script_lazypath);
        run_gen_grammar_tables6.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/extinst.opencl.debuginfo.100.grammar.json"),
        );
        const @"opencl.debuginfo.100.insts.inc" = run_gen_grammar_tables6.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "opencl.debuginfo.100.insts.inc",
        );
        run_gen_grammar_tables6.addArg("--vendor-operand-kind-prefix=CLDEBUG100_");

        const run_gen_grammar_tables7 = b.addSystemCommand(&.{ python_path });
        run_gen_grammar_tables7.addFileArg(script_lazypath);
        run_gen_grammar_tables7.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/extinst.nonsemantic.shader.debuginfo.100.grammar.json"),
        );
        const @"nonsemantic.shader.debuginfo.100.insts.inc" = run_gen_grammar_tables7.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "nonsemantic.shader.debuginfo.100.insts.inc",
        );
        run_gen_grammar_tables7.addArg("--vendor-operand-kind-prefix=SHDEBUG100_");

        const run_gen_grammar_tables8 = b.addSystemCommand(&.{ python_path });
        run_gen_grammar_tables8.addFileArg(script_lazypath);
        run_gen_grammar_tables8.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/extinst.nonsemantic.clspvreflection.grammar.json"),
        );
        const @"nonsemantic.clspvreflection.insts.inc" = run_gen_grammar_tables8.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "nonsemantic.clspvreflection.insts.inc",
        );
        run_gen_grammar_tables8.addArg("--vendor-operand-kind-prefix=");

        const run_gen_grammar_tables9 = b.addSystemCommand(&.{ python_path });
        run_gen_grammar_tables9.addFileArg(script_lazypath);
        run_gen_grammar_tables9.addPrefixedFileArg(
            "--extinst-glsl-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/extinst.glsl.std.450.grammar.json"),
        );
        const @"glsl.std.450.insts.inc" = run_gen_grammar_tables9.addPrefixedOutputFileArg(
            "--glsl-insts-output=",
            "glsl.std.450.insts.inc",
        );
        run_gen_grammar_tables9.addArg("--output-language=c++");

        const run_gen_grammar_tables10 = b.addSystemCommand(&.{ python_path });
        run_gen_grammar_tables10.addFileArg(script_lazypath);
        run_gen_grammar_tables10.addPrefixedFileArg(
            "--extinst-opencl-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/extinst.opencl.std.100.grammar.json"),
        );
        const @"opencl.std.insts.inc" = run_gen_grammar_tables10.addPrefixedOutputFileArg(
            "--opencl-insts-output=",
            "opencl.std.insts.inc",
        );

        const run_gen_grammar_tables11 = b.addSystemCommand(&.{ python_path });
        run_gen_grammar_tables11.addFileArg(script_lazypath);
        run_gen_grammar_tables11.addPrefixedFileArg(
            "--spirv-core-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/spirv.core.grammar.json"),
        );
        run_gen_grammar_tables11.addPrefixedFileArg(
            "--extinst-debuginfo-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/extinst.debuginfo.grammar.json"),
        );
        run_gen_grammar_tables11.addPrefixedFileArg(
            "--extinst-cldebuginfo100-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/extinst.opencl.debuginfo.100.grammar.json"),
        );
        const @"core.insts-unified1.inc" = run_gen_grammar_tables11.addPrefixedOutputFileArg(
            "--core-insts-output=",
            "core.insts-unified1.inc",
        );
        const @"operand.kinds-unified1.inc" = run_gen_grammar_tables11.addPrefixedOutputFileArg(
            "--operand-kinds-output=",
            "operand.kinds-unified1.inc",
        );
        run_gen_grammar_tables11.addArg("--output-language=c++");

        const run_gen_grammar_tables12 = b.addSystemCommand(&.{ python_path });
        run_gen_grammar_tables12.addFileArg(script_lazypath);
        run_gen_grammar_tables12.addPrefixedFileArg(
            "--spirv-core-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/spirv.core.grammar.json"),
        );
        run_gen_grammar_tables12.addPrefixedFileArg(
            "--extinst-debuginfo-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/extinst.debuginfo.grammar.json"),
        );
        run_gen_grammar_tables12.addPrefixedFileArg(
            "--extinst-cldebuginfo100-grammar=",
            spirv_headers_dep.path("include/spirv/unified1/extinst.opencl.debuginfo.100.grammar.json"),
        );
        const @"extension_enum.inc" = run_gen_grammar_tables12.addPrefixedOutputFileArg(
            "--extension-enum-output=",
            "extension_enum.inc",
        );
        const @"enum_string_mapping.inc" = run_gen_grammar_tables12.addPrefixedOutputFileArg(
            "--enum-string-mapping-output=",
            "enum_string_mapping.inc",
        );
        run_gen_grammar_tables12.addArg("--output-language=c++");

        const run_gen_grammar_tables13 = b.addSystemCommand(&.{ python_path });
        run_gen_grammar_tables13.addFileArg(script_lazypath);
        run_gen_grammar_tables13.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            spirv_headers_dep.path(
                "include/spirv/unified1/extinst.nonsemantic.vkspreflection.grammar.json",
            ),
        );
        const @"nonsemantic.vkspreflection.insts.inc" = run_gen_grammar_tables13.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "nonsemantic.vkspreflection.insts.inc",
        );
        run_gen_grammar_tables13.addArg("--vendor-operand-kind-prefix=");

        run_compare6.addFileArg(@"spv-amd-shader-explicit-vertex-parameter.insts.inc");
        run_compare7.addFileArg(@"spv-amd-shader-trinary-minmax.insts.inc");
        run_compare8.addFileArg(@"spv-amd-gcn-shader.insts.inc");
        run_compare9.addFileArg(@"spv-amd-shader-ballot.insts.inc");
        run_compare10.addFileArg(@"debuginfo.insts.inc");
        run_compare11.addFileArg(@"opencl.debuginfo.100.insts.inc");
        run_compare12.addFileArg(@"nonsemantic.shader.debuginfo.100.insts.inc");
        run_compare13.addFileArg(@"nonsemantic.clspvreflection.insts.inc");
        run_compare14.addFileArg(@"core.insts-unified1.inc");
        run_compare15.addFileArg(@"operand.kinds-unified1.inc");
        run_compare16.addFileArg(@"extension_enum.inc");
        run_compare17.addFileArg(@"enum_string_mapping.inc");
        run_compare18.addFileArg(@"glsl.std.450.insts.inc");
        run_compare19.addFileArg(@"opencl.std.insts.inc");
        run_compare20.addFileArg(@"nonsemantic.vkspreflection.insts.inc");

        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"spv-amd-shader-explicit-vertex-parameter.insts.inc",
                .header,
                "python/spv-amd-shader-explicit-vertex-parameter.insts.inc",
            ).step
        );
        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"spv-amd-shader-trinary-minmax.insts.inc",
                .header,
                "python/spv-amd-shader-trinary-minmax.insts.inc",
            ).step
        );
        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"spv-amd-gcn-shader.insts.inc",
                .header,
                "python/spv-amd-gcn-shader.insts.inc",
            ).step
        );
        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"spv-amd-shader-ballot.insts.inc",
                .header,
                "python/spv-amd-shader-ballot.insts.inc",
            ).step
        );
        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"debuginfo.insts.inc",
                .header,
                "python/debuginfo.insts.inc",
            ).step
        );
        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"opencl.debuginfo.100.insts.inc",
                .header,
                "python/opencl.debuginfo.100.insts.inc",
            ).step
        );
        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"nonsemantic.shader.debuginfo.100.insts.inc",
                .header,
                "python/nonsemantic.shader.debuginfo.100.insts.inc",
            ).step
        );
        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"nonsemantic.clspvreflection.insts.inc",
                .header,
                "python/nonsemantic.clspvreflection.insts.inc",
            ).step
        );
        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"core.insts-unified1.inc",
                .header,
                "python/core.insts-unified1.inc",
            ).step
        );
        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"operand.kinds-unified1.inc",
                .header,
                "python/operand.kinds-unified1.inc",
            ).step
        );
        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"extension_enum.inc",
                .header,
                "python/extension_enum.inc",
            ).step
        );
        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"enum_string_mapping.inc",
                .header,
                "python/enum_string_mapping.inc",
            ).step
        );
        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"glsl.std.450.insts.inc",
                .header,
                "python/glsl.std.450.insts.inc",
            ).step
        );
        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"opencl.std.insts.inc",
                .header,
                "python/opencl.std.insts.inc",
            ).step
        );
        install_step.dependOn(
            &b.addInstallFileWithDir(
                @"nonsemantic.vkspreflection.insts.inc",
                .header,
                "python/nonsemantic.vkspreflection.insts.inc",
            ).step
        );
    }
}
