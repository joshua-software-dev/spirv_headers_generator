const std = @import("std");


fn read_file_sha256(file: std.fs.File, out_buffer: *[32]u8) !void {
    var hasher = std.crypto.hash.sha2.Sha256.init(.{});

    var buffer: [4096]u8 = undefined;
    while (true) {
        const amt = try file.read(buffer[0..]);
        if (amt == 0) {
            hasher.final(out_buffer);
            return;
        }

        hasher.update(buffer[0..amt]);
    }
}

pub fn main() !u8 {
    var gpa: std.heap.GeneralPurposeAllocator(.{}) = .{};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    const args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);

    const lhs_file_path = args[1];
    const rhs_file_path = args[2];

    var lhs_out: [32]u8 = undefined;
    var rhs_out: [32]u8 = undefined;
    {
        const cwd = std.fs.cwd();
        const lhs_file = try cwd.openFile(lhs_file_path, .{});
        defer lhs_file.close();

        const rhs_file = try cwd.openFile(rhs_file_path, .{});
        defer rhs_file.close();

        try read_file_sha256(lhs_file, &lhs_out);
        try read_file_sha256(rhs_file, &rhs_out);
    }

    if (!std.mem.eql(u8, &lhs_out, &rhs_out)) {
        std.debug.print("\nERROR| {s}: {x}\n{s}: {x}\n", .{
            lhs_file_path,
            std.fmt.fmtSliceHexLower(&lhs_out),
            rhs_file_path,
            std.fmt.fmtSliceHexLower(&rhs_out),
        });
        return 1;
    }

    try std.io.getStdOut().writer().print("\n{s}=={s}|{x}\n", .{
        std.fs.path.basename(lhs_file_path),
        std.fs.path.basename(rhs_file_path),
        std.fmt.fmtSliceHexLower(&lhs_out),
    });

    return 0;
}
