# SPIRV-Headers Generator

A port of the python scripts used to generate headers during the build of SPIRV-Headers/SPIRV-Tools to zig.

```zig
pub fn build(b: *std.Build) !void {
    // ...

    const spirv_headers_dep = b.dependency("spirv_headers", .{});
    const spirv_tools_dep = b.dependency("spirv_tools", .{});
    const generator_dep = b.dependency("spirv_headers_generator", .{
        .target = target,
        .optimize = optimize,
        .spirv_headers_path = @as([]const u8, spirv_headers_dep.path("").getPath(b)),
        .spirv_tools_path = @as([]const u8, spirv_tools_dep.path("").getPath(b)),
        // When using the zig package manager with SPIRV-Tools, it will not
        // include the .git folder in the cloned repo. One of the generator
        // scripts runs git commands to obtain the current hash of the repo,
        // and these commands will fail without that folder and its metadata
        // being present. You can specify the commit manually here, or let it
        // fallback as usual to the default of: "unknown hash".
        .spirv_tools_git_hash_override = @as([]const u8, "51892874ba08f3ac0d9b1fcf3893c8516693a88e"),
    });
    const spirv_headers_generated = generator_dep.namedWriteFiles("spirv_headers_generated");

    const my_exe = b.addExecutable(...);
    my_exe.addIncludePath(spirv_headers_generated.getDirectory());

    // ...
}
```
