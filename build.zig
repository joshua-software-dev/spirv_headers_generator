const std = @import("std");

const tzonp = @import("src/terrible_zon_parser.zig");


pub fn get_spirv_tools_git_hash(b: *std.Build, zon_path: []const u8) !u256 {
    const zon = try tzonp.parse_zon_from_file_path(b.allocator, zon_path);
    defer zon.deinit();

    for (zon.value.dependencies) |dep| {
        if (!std.mem.eql(u8, dep.name, "spirv_tools")) continue;

        const raw_url = switch (dep.kind) {
            .url => |url| url.url,
            else => return error.InvalidZon,
        };

        if (std.mem.startsWith(u8, raw_url, "git+https://")) {
            const basename = std.fs.path.basenamePosix(raw_url);
            if (std.mem.indexOf(u8, basename, "#")) |found_pos| {
                return std.fmt.parseInt(u256, basename[found_pos + 1..], 16);
            }

            return error.InvalidZonDependencyUrl;
        } else {
            const archive_name = std.fs.path.basenamePosix(raw_url);
            if (std.mem.endsWith(u8, archive_name, ".tar")) {
                return std.fmt.parseInt(u256, std.fs.path.stem(archive_name), 16);
            } else if (
                std.mem.endsWith(u8, archive_name, ".tar.gz") or
                std.mem.endsWith(u8, archive_name, ".tar.xz") or
                std.mem.endsWith(u8, archive_name, ".tar.zst")
            ) {
                return std.fmt.parseInt(u256, std.fs.path.stem(std.fs.path.stem(archive_name)), 16);
            } else {
                return error.InvalidZonDependencyUrl;
            }
        }
    }

    return error.InvalidZon;
}

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    const spirv_headers_abspath = b.option(
        []const u8,
        "spirv_headers_path",
        "Path to a SPIRV-Headers repo location to use when generating files",
    )
        orelse return error.OptionRequired;
    const spirv_tools_abspath = b.option(
        []const u8,
        "spirv_tools_path",
        "Path to a SPIRV-Tools repo location to use when generating files",
    )
        orelse return error.OptionRequired;
    const spirv_tools_git_hash_override = b.option(
        []const u8,
        "spirv_tools_git_hash_override",
        "Override the result of the git hash lookup made when generating files with this string",
    )
        orelse "";

    const zig_xml_dep = b.dependency("zig_xml", .{ .target = b.host, .optimize = optimize });
    const yazap_dep = b.dependency("yazap", .{ .target = b.host, .optimize = optimize });

    const spirv_headers_relpath = try std.fs.path.relative(
        b.allocator,
        b.build_root.path orelse return error.InvalidBuildRoot,
        spirv_headers_abspath,
    );
    defer b.allocator.free(spirv_headers_relpath);
    const spirv_tools_relpath = try std.fs.path.relative(
        b.allocator,
        b.build_root.path orelse return error.InvalidBuildRoot,
        spirv_tools_abspath,
    );
    defer b.allocator.free(spirv_tools_relpath);

    const all_headers = b.addNamedWriteFiles("spirv_headers_generated");
    const spirv_headers_generated = b.addStaticLibrary(.{
        .name = "spirv_headers_generated",
        .root_source_file = b.addWriteFiles().add("empty.c", ""),
        .target = target,
        .optimize = optimize,
    });
    b.installArtifact(spirv_headers_generated);

    {
        const update_build_version = b.addExecutable(.{
            .name = "update_build_version",
            .root_source_file = b.path("src/update_build_version.zig"),
            .target = b.host,
            .optimize = optimize,
        });
        update_build_version.root_module.addImport("yazap", yazap_dep.module("yazap"));

        const run_update_build_ver = b.addRunArtifact(update_build_version);
        run_update_build_ver.addFileArg(b.path(b.pathJoin(&.{ spirv_tools_relpath, "CHANGES" })));
        const @"build-version.inc" = run_update_build_ver.addOutputFileArg("build-version.inc");
        run_update_build_ver.setEnvironmentVariable("SOURCE_DATE_EPOCH", "0");
        if (spirv_tools_git_hash_override.len > 0) {
            run_update_build_ver.setEnvironmentVariable("GIT_HASH_OVERRIDE", spirv_tools_git_hash_override);
        }

        b.installArtifact(update_build_version);

        spirv_headers_generated.installHeader(@"build-version.inc", "build-version.inc");
        _ = all_headers.addCopyFile(@"build-version.inc", "build-version.inc");
    }

    {
        const generate_registry_tables = b.addExecutable(.{
            .name = "generate_registry_tables",
            .root_source_file = b.path("src/generate_registry_tables.zig"),
            .target = b.host,
            .optimize = optimize,
        });
        generate_registry_tables.root_module.addImport("yazap", yazap_dep.module("yazap"));
        generate_registry_tables.root_module.addImport("zig-xml", zig_xml_dep.module("xml"));

        const run_gen_reg_tables = b.addRunArtifact(generate_registry_tables);
        run_gen_reg_tables.addPrefixedFileArg(
            "--xml=",
            b.path(b.pathJoin(&.{ spirv_headers_relpath, "include/spirv/spir-v.xml" })),
        );
        const @"generators.inc" = run_gen_reg_tables.addPrefixedOutputFileArg("--generator-output=", "generators.inc");

        b.installArtifact(generate_registry_tables);

        spirv_headers_generated.installHeader(@"generators.inc", "generators.inc");
        _ = all_headers.addCopyFile(@"generators.inc", "generators.inc");
    }

    {
        const generate_language_headers = b.addExecutable(.{
            .name = "generate_language_headers",
            .root_source_file = b.path("src/generate_language_headers.zig"),
            .target = b.host,
            .optimize = optimize,
        });
        generate_language_headers.root_module.addImport("yazap", yazap_dep.module("yazap"));

        const run_gen_lang_headers1 = b.addRunArtifact(generate_language_headers);
        run_gen_lang_headers1.addPrefixedFileArg(
            "--extinst-grammar=",
            b.path(b.pathJoin(&.{ spirv_headers_relpath, "include/spirv/unified1/extinst.debuginfo.grammar.json" })),
        );
        const @"DebugInfo.h" = run_gen_lang_headers1.addPrefixedOutputFileArg(
            "--extinst-output-path=",
            "DebugInfo.h",
        );

        const run_gen_lang_headers2 = b.addRunArtifact(generate_language_headers);
        run_gen_lang_headers2.addPrefixedFileArg(
            "--extinst-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.nonsemantic.shader.debuginfo.100.grammar.json",
            })),
        );
        const @"NonSemanticShaderDebugInfo100.h" = run_gen_lang_headers2.addPrefixedOutputFileArg(
            "--extinst-output-path=",
            "NonSemanticShaderDebugInfo100.h",
        );

        const run_gen_lang_headers3 = b.addRunArtifact(generate_language_headers);
        run_gen_lang_headers3.addPrefixedFileArg(
            "--extinst-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.opencl.debuginfo.100.grammar.json",
            })),
        );
        const @"OpenCLDebugInfo100.h" = run_gen_lang_headers3.addPrefixedOutputFileArg(
            "--extinst-output-path=",
            "OpenCLDebugInfo100.h",
        );

        b.installArtifact(generate_language_headers);

        spirv_headers_generated.installHeader(@"DebugInfo.h", "DebugInfo.h");
        _ = all_headers.addCopyFile(@"DebugInfo.h", "DebugInfo.h");

        spirv_headers_generated.installHeader(@"NonSemanticShaderDebugInfo100.h", "NonSemanticShaderDebugInfo100.h");
        _ = all_headers.addCopyFile(@"NonSemanticShaderDebugInfo100.h", "NonSemanticShaderDebugInfo100.h");

        spirv_headers_generated.installHeader(@"OpenCLDebugInfo100.h", "OpenCLDebugInfo100.h");
        _ = all_headers.addCopyFile(@"OpenCLDebugInfo100.h", "OpenCLDebugInfo100.h");
    }

    {
        const generate_grammar_tables = b.addExecutable(.{
            .name = "generate_grammar_tables",
            .root_source_file = b.path("src/generate_grammar_tables.zig"),
            .target = b.host,
            .optimize = optimize,
        });
        generate_grammar_tables.root_module.addImport("yazap", yazap_dep.module("yazap"));

        const run_gen_grammar_tables1 = b.addRunArtifact(generate_grammar_tables);
        run_gen_grammar_tables1.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.spv-amd-shader-explicit-vertex-parameter.grammar.json",
            })),
        );
        const @"spv-amd-shader-explicit-vertex-parameter.insts.inc" = run_gen_grammar_tables1.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "spv-amd-shader-explicit-vertex-parameter.insts.inc",
        );
        run_gen_grammar_tables1.addArg("--vendor-operand-kind-prefix=");

        const run_gen_grammar_tables2 = b.addRunArtifact(generate_grammar_tables);
        run_gen_grammar_tables2.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.spv-amd-shader-trinary-minmax.grammar.json",
            })),
        );
        const @"spv-amd-shader-trinary-minmax.insts.inc" = run_gen_grammar_tables2.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "spv-amd-shader-trinary-minmax.insts.inc",
        );
        run_gen_grammar_tables2.addArg("--vendor-operand-kind-prefix=");

        const run_gen_grammar_tables3 = b.addRunArtifact(generate_grammar_tables);
        run_gen_grammar_tables3.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.spv-amd-gcn-shader.grammar.json",
            })),
        );
        const @"spv-amd-gcn-shader.insts.inc" = run_gen_grammar_tables3.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "spv-amd-gcn-shader.insts.inc",
        );
        run_gen_grammar_tables3.addArg("--vendor-operand-kind-prefix=");

        const run_gen_grammar_tables4 = b.addRunArtifact(generate_grammar_tables);
        run_gen_grammar_tables4.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.spv-amd-shader-ballot.grammar.json",
            })),
        );
        const @"spv-amd-shader-ballot.insts.inc" = run_gen_grammar_tables4.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "spv-amd-shader-ballot.insts.inc",
        );
        run_gen_grammar_tables4.addArg("--vendor-operand-kind-prefix=");

        const run_gen_grammar_tables5 = b.addRunArtifact(generate_grammar_tables);
        run_gen_grammar_tables5.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.debuginfo.grammar.json",
            })),
        );
        const @"debuginfo.insts.inc" = run_gen_grammar_tables5.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "debuginfo.insts.inc",
        );
        run_gen_grammar_tables5.addArg("--vendor-operand-kind-prefix=");

        const run_gen_grammar_tables6 = b.addRunArtifact(generate_grammar_tables);
        run_gen_grammar_tables6.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.opencl.debuginfo.100.grammar.json",
            })),
        );
        const @"opencl.debuginfo.100.insts.inc" = run_gen_grammar_tables6.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "opencl.debuginfo.100.insts.inc",
        );
        run_gen_grammar_tables6.addArg("--vendor-operand-kind-prefix=CLDEBUG100_");

        const run_gen_grammar_tables7 = b.addRunArtifact(generate_grammar_tables);
        run_gen_grammar_tables7.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.nonsemantic.shader.debuginfo.100.grammar.json",
            })),
        );
        const @"nonsemantic.shader.debuginfo.100.insts.inc" = run_gen_grammar_tables7.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "nonsemantic.shader.debuginfo.100.insts.inc",
        );
        run_gen_grammar_tables7.addArg("--vendor-operand-kind-prefix=SHDEBUG100_");

        const run_gen_grammar_tables8 = b.addRunArtifact(generate_grammar_tables);
        run_gen_grammar_tables8.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.nonsemantic.clspvreflection.grammar.json",
            })),
        );
        const @"nonsemantic.clspvreflection.insts.inc" = run_gen_grammar_tables8.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "nonsemantic.clspvreflection.insts.inc",
        );
        run_gen_grammar_tables8.addArg("--vendor-operand-kind-prefix=");

        const run_gen_grammar_tables9 = b.addRunArtifact(generate_grammar_tables);
        run_gen_grammar_tables9.addPrefixedFileArg(
            "--extinst-glsl-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.glsl.std.450.grammar.json",
            })),
        );
        const @"glsl.std.450.insts.inc" = run_gen_grammar_tables9.addPrefixedOutputFileArg(
            "--glsl-insts-output=",
            "glsl.std.450.insts.inc",
        );
        run_gen_grammar_tables9.addArg("--output-language=c++");

        const run_gen_grammar_tables10 = b.addRunArtifact(generate_grammar_tables);
        run_gen_grammar_tables10.addPrefixedFileArg(
            "--extinst-opencl-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.opencl.std.100.grammar.json",
            })),
        );
        const @"opencl.std.insts.inc" = run_gen_grammar_tables10.addPrefixedOutputFileArg(
            "--opencl-insts-output=",
            "opencl.std.insts.inc",
        );

        const run_gen_grammar_tables11 = b.addRunArtifact(generate_grammar_tables);
        run_gen_grammar_tables11.addPrefixedFileArg(
            "--spirv-core-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/spirv.core.grammar.json",
            })),
        );
        run_gen_grammar_tables11.addPrefixedFileArg(
            "--extinst-debuginfo-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.debuginfo.grammar.json",
            })),
        );
        run_gen_grammar_tables11.addPrefixedFileArg(
            "--extinst-cldebuginfo100-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.opencl.debuginfo.100.grammar.json",
            })),
        );
        const @"core.insts-unified1.inc" = run_gen_grammar_tables11.addPrefixedOutputFileArg(
            "--core-insts-output=",
            "extension_enum.inc",
        );
        const @"operand.kinds-unified1.inc" = run_gen_grammar_tables11.addPrefixedOutputFileArg(
            "--operand-kinds-output=",
            "enum_string_mapping.inc",
        );
        run_gen_grammar_tables11.addArg("--output-language=c++");

        const run_gen_grammar_tables12 = b.addRunArtifact(generate_grammar_tables);
        run_gen_grammar_tables12.addPrefixedFileArg(
            "--spirv-core-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/spirv.core.grammar.json",
            })),
        );
        run_gen_grammar_tables12.addPrefixedFileArg(
            "--extinst-debuginfo-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.debuginfo.grammar.json",
            })),
        );
        run_gen_grammar_tables12.addPrefixedFileArg(
            "--extinst-cldebuginfo100-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.opencl.debuginfo.100.grammar.json",
            })),
        );
        const @"extension_enum.inc" = run_gen_grammar_tables12.addPrefixedOutputFileArg(
            "--extension-enum-output=",
            "extension_enum.inc",
        );
        const @"enum_string_mapping.inc" = run_gen_grammar_tables12.addPrefixedOutputFileArg(
            "--enum-string-mapping-output=",
            "enum_string_mapping.inc",
        );
        run_gen_grammar_tables12.addArg("--output-language=c++");

        const run_gen_grammar_tables13 = b.addRunArtifact(generate_grammar_tables);
        run_gen_grammar_tables13.addPrefixedFileArg(
            "--extinst-vendor-grammar=",
            b.path(b.pathJoin(&.{
                spirv_headers_relpath,
                "include/spirv/unified1/extinst.nonsemantic.vkspreflection.grammar.json",
            })),
        );
        const @"nonsemantic.vkspreflection.insts.inc" = run_gen_grammar_tables13.addPrefixedOutputFileArg(
            "--vendor-insts-output=",
            "nonsemantic.vkspreflection.insts.inc",
        );
        run_gen_grammar_tables13.addArg("--vendor-operand-kind-prefix=");

        b.installArtifact(generate_grammar_tables);

        // 1
        spirv_headers_generated.installHeader(@"spv-amd-shader-explicit-vertex-parameter.insts.inc", "spv-amd-shader-explicit-vertex-parameter.insts.inc");
        _ = all_headers.addCopyFile(@"spv-amd-shader-explicit-vertex-parameter.insts.inc", "spv-amd-shader-explicit-vertex-parameter.insts.inc");

        // 2
        spirv_headers_generated.installHeader(@"spv-amd-shader-trinary-minmax.insts.inc", "spv-amd-shader-trinary-minmax.insts.inc");
        _ = all_headers.addCopyFile(@"spv-amd-shader-trinary-minmax.insts.inc", "spv-amd-shader-trinary-minmax.insts.inc");

        // 3
        spirv_headers_generated.installHeader(@"spv-amd-gcn-shader.insts.inc", "spv-amd-gcn-shader.insts.inc");
        _ = all_headers.addCopyFile(@"spv-amd-gcn-shader.insts.inc", "spv-amd-gcn-shader.insts.inc");

        // 4
        spirv_headers_generated.installHeader(@"spv-amd-shader-ballot.insts.inc", "spv-amd-shader-ballot.insts.inc");
        _ = all_headers.addCopyFile(@"spv-amd-shader-ballot.insts.inc", "spv-amd-shader-ballot.insts.inc");

        // 5
        spirv_headers_generated.installHeader(@"debuginfo.insts.inc", "debuginfo.insts.inc");
        _ = all_headers.addCopyFile(@"debuginfo.insts.inc", "debuginfo.insts.inc");

        // 6
        spirv_headers_generated.installHeader(@"opencl.debuginfo.100.insts.inc", "opencl.debuginfo.100.insts.inc");
        _ = all_headers.addCopyFile(@"opencl.debuginfo.100.insts.inc", "opencl.debuginfo.100.insts.inc");

        // 7
        spirv_headers_generated.installHeader(@"nonsemantic.shader.debuginfo.100.insts.inc", "nonsemantic.shader.debuginfo.100.insts.inc");
        _ = all_headers.addCopyFile(@"nonsemantic.shader.debuginfo.100.insts.inc", "nonsemantic.shader.debuginfo.100.insts.inc");

        // 8
        spirv_headers_generated.installHeader(@"nonsemantic.clspvreflection.insts.inc", "nonsemantic.clspvreflection.insts.inc");
        _ = all_headers.addCopyFile(@"nonsemantic.clspvreflection.insts.inc", "nonsemantic.clspvreflection.insts.inc");

        // 9
        spirv_headers_generated.installHeader(@"core.insts-unified1.inc", "core.insts-unified1.inc");
        _ = all_headers.addCopyFile(@"core.insts-unified1.inc", "core.insts-unified1.inc");

        // 10
        spirv_headers_generated.installHeader(@"operand.kinds-unified1.inc", "operand.kinds-unified1.inc");
        _ = all_headers.addCopyFile(@"operand.kinds-unified1.inc", "operand.kinds-unified1.inc");

        // 11
        spirv_headers_generated.installHeader(@"extension_enum.inc", "extension_enum.inc");
        _ = all_headers.addCopyFile(@"extension_enum.inc", "extension_enum.inc");

        // 12
        spirv_headers_generated.installHeader(@"enum_string_mapping.inc", "enum_string_mapping.inc");
        _ = all_headers.addCopyFile(@"enum_string_mapping.inc", "enum_string_mapping.inc");

        // 13
        spirv_headers_generated.installHeader(@"glsl.std.450.insts.inc", "glsl.std.450.insts.inc");
        _ = all_headers.addCopyFile(@"glsl.std.450.insts.inc", "glsl.std.450.insts.inc");

        // 14
        spirv_headers_generated.installHeader(@"opencl.std.insts.inc", "opencl.std.insts.inc");
        _ = all_headers.addCopyFile(@"opencl.std.insts.inc", "opencl.std.insts.inc");

        // 15
        spirv_headers_generated.installHeader(@"nonsemantic.vkspreflection.insts.inc", "nonsemantic.vkspreflection.insts.inc");
        _ = all_headers.addCopyFile(@"nonsemantic.vkspreflection.insts.inc", "nonsemantic.vkspreflection.insts.inc");
    }
}
