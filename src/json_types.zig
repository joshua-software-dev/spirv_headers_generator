const std = @import("std");


pub const Operand = struct {
    kind: []const u8,
    name: ?[]const u8 = null,
    quantifier: ?[]const u8 = null,
};

pub const Instruction = struct {
    opname: []const u8,
    opcode: u32,
    operands: ?[]const Operand = null,
    capabilities: ?[]const []const u8 = null,
    capability: ?[]const u8 = null,
    extensions: ?[]const []const u8 = null,

    class: ?[]const u8 = null,
    lastVersion: ?[]const u8 = null,
    version: ?[]const u8 = null,

    pub fn dupe(
        self: @This(),
        allocator: std.mem.Allocator,
        prefixed_operand_replacement_map: std.StringHashMap([]const u8),
    ) !@This() {
        return .{
            .opname = try allocator.dupe(u8, self.opname),
            .opcode = self.opcode,
            .class = if (self.class) |c| try allocator.dupe(u8, c) else null,
            .lastVersion = if (self.lastVersion) |lv| try allocator.dupe(u8, lv) else null,
            .version = if (self.version) |v| try allocator.dupe(u8, v) else null,
            .operands = blk: {
                if (self.operands) |operands| {
                    var buffer = try std.ArrayListUnmanaged(Operand).initCapacity(allocator, operands.len);
                    for (operands) |op| {
                        buffer.appendAssumeCapacity(.{
                            .kind = blk2: {
                                if (prefixed_operand_replacement_map.get(op.kind)) |replacement| {
                                    break :blk2 try allocator.dupe(u8, replacement);
                                }

                                break :blk2 try allocator.dupe(u8, op.kind);
                            },
                            .name = if (op.name) |n| try allocator.dupe(u8, n) else null,
                            .quantifier = if (op.quantifier) |q| try allocator.dupe(u8, q) else null,
                        });
                    }
                    break :blk try buffer.toOwnedSlice(allocator);
                }

                break :blk null;
            },
            .capabilities = blk: {
                if (self.capabilities) |capabilities| {
                    var buffer = try std.ArrayListUnmanaged([]const u8).initCapacity(allocator, capabilities.len);
                    for (capabilities) |cap| {
                        buffer.appendAssumeCapacity(try allocator.dupe(u8, cap));
                    }
                    break :blk try buffer.toOwnedSlice(allocator);
                }

                break :blk null;
            },
            .capability = if (self.capability) |c| try allocator.dupe(u8, c) else null,
            .extensions = blk: {
                if (self.extensions) |extensions| {
                    var buffer = try std.ArrayListUnmanaged([]const u8).initCapacity(allocator, extensions.len);
                    for (extensions) |ext| {
                        buffer.appendAssumeCapacity(try allocator.dupe(u8, ext));
                    }
                    break :blk try buffer.toOwnedSlice(allocator);
                }

                break :blk null;
            },
        };
    }
};

pub const Parameters = struct {
    kind: []const u8,

    name: ?[]const u8 = null,
    quantifier: ?[]const u8 = null,
};

pub const MaybeQuotedNumber = struct
{
    num: u64,
    raw: []const u8,

    pub fn jsonParse
    (
        allocator: std.mem.Allocator,
        source: anytype,
        options: std.json.ParseOptions,
    )
    std.json.ParseError(@TypeOf(source.*))!@This()
    {
        _ = options;

        const token: std.json.Token = try source.next();
        switch (token)
        {
            .allocated_number, .allocated_string, .number, .string => |num_string| {
                return .{
                    .num = try std.fmt.parseInt(u64, num_string, 0),
                    .raw = try allocator.dupe(u8, num_string)
                };
            },
            .partial_number, .partial_string => |partial_num_string| {
                var buffer = std.ArrayList(u8).init(allocator);
                defer buffer.deinit();
                try buffer.appendSlice(partial_num_string);

                switch (try source.next()) {
                    .allocated_number, .allocated_string, .number, .string => |num_string| {
                        try buffer.appendSlice(num_string);
                        const num = try std.fmt.parseInt(u64, buffer.items, 0);
                        return .{
                            .num = num,
                            .raw = try buffer.toOwnedSlice()
                        };
                    },
                    else => unreachable,
                }
            },
            else => unreachable,
        }
    }
};

pub const Enumerant = struct {
    enumerant: []const u8,
    value: ?MaybeQuotedNumber = null,
    parameters: ?[]Parameters = null,

    capabilities: ?[]const []const u8 = null,
    extensions: ?[]const []const u8 = null,
    lastVersion: ?[]const u8 = null,
    version: ?[]const u8 = null,
};

pub const OperandKind = struct {
    category: []const u8,
    kind: []const u8,
    enumerants: ?[]const Enumerant = null,

    bases: ?[]const []const u8 = null,
    doc: ?[]const u8 = null,
    lastVersion: ?[]const u8 = null,
    version: ?[]const u8 = null,

    pub fn dupe(self: @This(), kind: ?[]const u8, enumerants: ?[]const Enumerant) @This() {
        return .{
            .bases = self.bases,
            .category = self.category,
            .doc = self.doc,
            .enumerants = enumerants orelse self.enumerants,
            .kind = kind orelse self.kind,
            .lastVersion = self.lastVersion,
            .version = self.version,
        };
    }
};

pub const InstructionPrintingClass = struct {
    tag: []const u8,
    heading: ?[]const u8 = null,
};

pub const Grammar = struct {
    copyright: ?[]const []const u8 = null,
    version: ?u32 = null,
    revision: u32,
    instructions: []const Instruction,
    operand_kinds: ?[]const OperandKind = null,

    instruction_printing_class: ?[]const InstructionPrintingClass = null,
    magic_number: ?[]const u8 = null,
    major_version: ?u32 = null,
    minor_version: ?u32 = null,

    pub fn dupe_instructions_with_prefix(
        self: @This(),
        allocator: std.mem.Allocator,
        prefix: ?[]const u8,
    ) !std.json.Parsed([]const Instruction) {
        var container: std.json.Parsed([]const Instruction) = .{
            .arena = try allocator.create(std.heap.ArenaAllocator),
            .value = undefined,
        };
        errdefer allocator.destroy(container.arena);
        container.arena.* = std.heap.ArenaAllocator.init(allocator);

        container.value = blk: {
            var operand_arena = std.heap.ArenaAllocator.init(allocator);
            defer operand_arena.deinit();
            var prefixed_operand_replacement_map = std.StringHashMap([]const u8).init(operand_arena.allocator());
            defer prefixed_operand_replacement_map.deinit();
            if (self.operand_kinds) |op_kinds| {
                if (prefix) |p| {
                    for (op_kinds) |op_kind| {
                        try prefixed_operand_replacement_map.put(
                            op_kind.kind,
                            try std.fmt.allocPrint(
                                operand_arena.allocator(),
                                "{s}{s}",
                                .{ p, op_kind.kind },
                            )
                        );
                    }
                }
            }

            var buffer = try std.ArrayList(Instruction).initCapacity(container.arena.allocator(), self.instructions.len);
            defer buffer.deinit();
            for (self.instructions) |instruction| {
                buffer.appendAssumeCapacity(
                    try instruction.dupe(container.arena.allocator(), prefixed_operand_replacement_map)
                );
            }

            std.sort.pdq(
                Instruction,
                buffer.items,
                {},
                struct {
                    pub fn lessThan(
                        _: void,
                        lhs: Instruction,
                        rhs: Instruction,
                    ) bool {
                        if (lhs.opcode == rhs.opcode) {
                            return std.mem.lessThan(u8, lhs.opname, rhs.opname);
                        }

                        return lhs.opcode < rhs.opcode;
                    }
                }.lessThan,
            );

            break :blk try buffer.toOwnedSlice();
        };

        return container;
    }
};
