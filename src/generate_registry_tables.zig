const std = @import("std");

const yazap = @import("yazap");
const zig_xml = @import("zig-xml");


const Vendor = struct {
    id: u32,
    vendor: []const u8,
    tool: ?[]const u8,
};

fn find_tags_by_attribute_recursive
(
    located: *std.ArrayList(zig_xml.Node.Element),
    attr_key: ?[]const u8,
    attr_value: ?[]const u8,
    element: zig_xml.Node.Element,
    first_only: bool,
)
!void
{
    for (element.children) |child|
    {
        switch (child)
        {
            .element => |e|
            {
                try find_tags_by_attribute_recursive(located, attr_key, attr_value, e, first_only);
            },
            .attribute => |a|
            {
                if (attr_key) |key|
                {
                    if (std.mem.eql(u8, key, a.name.local))
                    {
                        if (attr_value == null)
                        {
                            try located.append(element);
                            if (first_only) return error.FirstResultFound;
                        }
                        else if (std.mem.eql(u8, attr_value.?, a.value))
                        {
                            try located.append(element);
                            if (first_only) return error.FirstResultFound;
                        }
                    }
                }
                else if (attr_value) |value|
                {
                    if (std.mem.eql(u8, value, a.value))
                    {
                        try located.append(element);
                        if (first_only) return error.FirstResultFound;
                    }
                }
            },
            else => {},
        }
    }
}

fn find_tag_by_attribute
(
    attr_key: ?[]const u8,
    attr_value: ?[]const u8,
    element: zig_xml.Node.Element,
)
!?zig_xml.Node.Element
{
    var buf: [1]zig_xml.Node.Element = undefined;
    var located: std.ArrayList(zig_xml.Node.Element) = .{
        .items = buf[0..0],
        .capacity = buf.len,
        .allocator = undefined,
    };

    find_tags_by_attribute_recursive(&located, attr_key, attr_value, element, true)
        catch |err| switch (err)
        {
            error.FirstResultFound => return located.items[0],
            else => return err,
        };

    return null;
}

pub fn main() !u8 {
    var gpa: std.heap.GeneralPurposeAllocator(.{}) = .{};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    var app = yazap.App.init(
        allocator,
        "generate_registry_tables",
        "SPIRV-Tools/generate_registry_tables.py ported to zig",
    );
    defer app.deinit();
    var root_cmd = app.rootCommand();
    try root_cmd.addArg(yazap.Arg.singleValueOption("xml", null, null));
    try root_cmd.addArg(yazap.Arg.singleValueOption("generator-output", null, null));

    const matches = try app.parseProcess();
    if (!matches.containsArgs() or !(matches.containsArg("xml") and matches.containsArg("generator-output"))) {
        try app.displayHelp();
        return 1;
    }
    const xml_path_raw = matches.getSingleValue("xml").?;
    const output_file_path = matches.getSingleValue("generator-output").?;

    const output_dir_path = std.fs.path.dirname(output_file_path)
        orelse return error.InvalidOutputPath;
    const output_filename = std.fs.path.basename(output_file_path);

    // the current working directory does not need to be closed manually like
    // other directories, process exit will clean it up.
    const cwd = std.fs.cwd();
    const new_file_contents = blk: {
        const xml_file = try cwd.openFile(xml_path_raw, .{});

        var vendors = std.ArrayList(Vendor).init(allocator);
        defer vendors.deinit();

        var doc = try zig_xml.readDocument(allocator, xml_file.reader(), .{});
        defer doc.deinit();
        const container = (try find_tag_by_attribute("type", "vendor", doc.value.children[0].element))
            orelse return error.InvalidXml;
        for (container.children) |child| {
            switch (child) {
                .element => |e| {
                    if (std.mem.eql(u8, e.name.local, "id")) {
                        var id: ?u32 = null;
                        var vendor: ?[]const u8 = null;
                        var tool: ?[]const u8 = null;
                        for (e.children) |attributes| {
                            if (std.mem.eql(u8, attributes.attribute.name.local, "value")) {
                                id = try std.fmt.parseInt(u32, attributes.attribute.value, 10);
                            } else if (std.mem.eql(u8, attributes.attribute.name.local, "vendor")) {
                                vendor = attributes.attribute.value;
                            } else if (std.mem.eql(u8, attributes.attribute.name.local, "tool")) {
                                tool = attributes.attribute.value;
                            }
                        }
                        try vendors.append(.{
                            .id = id.?,
                            .vendor = vendor.?,
                            .tool = tool,
                        });
                    }
                },
                else => {},
            }
        }

        var output_buffer = std.ArrayList(u8).init(allocator);
        defer output_buffer.deinit();
        var out_writer = output_buffer.writer();
        var first_iter_complete = false;
        for (vendors.items) |vendor| {
            if (first_iter_complete) _ = try out_writer.write("\n");
            _ = try out_writer.write("{");
            try out_writer.print(
                \\{d}, "{s}", "{s}", "{s}
                ,
                .{
                    vendor.id,
                    vendor.vendor,
                    vendor.tool orelse "",
                    vendor.vendor,
                }
            );
            if (vendor.tool) |tool| try out_writer.print(" {s}", .{ tool });
            _ = try out_writer.write("\"},");
            first_iter_complete = true;
        }

        break :blk try output_buffer.toOwnedSlice();
    };
    defer allocator.free(new_file_contents);

    // cwd.openFile accepts relative and absolute paths
    const output_file = cwd.openFile(output_file_path, .{ .mode = .write_only })
        catch |err| switch (err) {
            error.FileNotFound => blk: {
                var output_dir = try cwd.makeOpenPath(output_dir_path, .{});
                defer output_dir.close();
                break :blk try output_dir.createFile(output_filename, .{ .mode = 0o644 });
            },
            else => return err,
        };
    defer output_file.close();

    try output_file.writeAll(new_file_contents);
    try output_file.setEndPos(new_file_contents.len);
    return 0;
}
