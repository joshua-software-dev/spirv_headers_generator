const std = @import("std");


const UrlDependency = struct {
    hash: []const u8,
    url: []const u8,
};

const DependencyKind = union(enum) {
    path: []const u8,
    url: UrlDependency,
};

const LazySetting = enum {
    not_set,
    null,
    true,
    false,
};

const Dependency = struct {
    kind: DependencyKind,
    lazy: LazySetting,
    name: []const u8,
};

const BuildZigZon = struct {
    name: []const u8,
    version: std.SemanticVersion,
    minimum_zig_version: ?std.SemanticVersion,
    dependencies: []const Dependency,
    paths: []const []const u8,

    pub fn format(
        obj: @This(),
        comptime fmt_str: []const u8,
        options: std.fmt.FormatOptions,
        writer: anytype,
    ) !void {
        _ = options;
        _ = fmt_str;
        try std.json.stringify(
            obj,
            .{ .whitespace = .indent_4 },
            writer,
        );
    }
};

fn parse_zon_string(
    arena_allocator: std.mem.Allocator,
    ast: std.zig.Ast,
    index: std.zig.Ast.Node.Index,
) ![]const u8 {
    if (ast.nodes.items(.tag)[index] != .string_literal) {
        return error.InvalidZon;
    }

    var temp_buffer = std.ArrayList(u8).init(arena_allocator);
    defer temp_buffer.deinit();

    const parse_result = try std.zig.string_literal.parseWrite(
        temp_buffer.writer(),
        ast.tokenSlice(ast.nodes.items(.main_token)[index]),
    );
    switch (parse_result) {
        .success => return temp_buffer.toOwnedSlice(),
        .failure => return error.InvalidStringLiteral,
    }
}

fn parse_zon_string_array(
    arena_allocator: std.mem.Allocator,
    ast: std.zig.Ast,
    index: std.zig.Ast.Node.Index,
) ![]const []const u8 {
    var zon_buf: [2]std.zig.Ast.Node.Index = undefined;
    const zon_array = ast.fullArrayInit(&zon_buf, index)
        orelse return error.InvalidZon;

    var output_buffer = try std.ArrayList([]const u8).initCapacity(arena_allocator, zon_array.ast.elements.len);
    defer output_buffer.deinit();

    for (zon_array.ast.elements) |elem_index| {
        try output_buffer.append(try parse_zon_string(arena_allocator, ast, elem_index));
    }

    return output_buffer.toOwnedSlice();
}

fn parse_zon_dependency(
    temp_allocator: std.mem.Allocator,
    arena_allocator: std.mem.Allocator,
    dependency_name: []const u8,
    ast: std.zig.Ast,
    index: std.zig.Ast.Node.Index,
) !Dependency {
    var zon_buf: [2]std.zig.Ast.Node.Index = undefined;
    const dependency_struct = ast.fullStructInit(&zon_buf, index)
        orelse return error.InvalidZon;

    var output_hash: ?[]const u8 = null;
    var output_lazy: LazySetting = .not_set;
    var output_path: ?[]const u8 = null;
    var output_url: ?[]const u8 = null;

    for (dependency_struct.ast.fields) |field_index| {
        const name_token_index = ast.firstToken(field_index) - 2;
        const raw_name = ast.tokenSlice(name_token_index);

        var temp_name_buffer = std.ArrayList(u8).init(temp_allocator);
        defer temp_name_buffer.deinit();
        const field_name = blk: {
            if (raw_name.len > 0 and raw_name[0] == '@') {
                switch (try std.zig.string_literal.parseWrite(temp_name_buffer.writer(), raw_name[1..])) {
                    .success => break :blk temp_name_buffer.items,
                    .failure => return error.InvalidStringLiteral,
                }
            }

            break :blk raw_name;
        };

        if (std.mem.eql(u8, field_name, "hash")) {
            output_hash = try parse_zon_string(arena_allocator, ast, field_index);
        } else if (std.mem.eql(u8, field_name, "url")) {
            output_url = try parse_zon_string(arena_allocator, ast, field_index);
        } else if (std.mem.eql(u8, field_name, "path")) {
            output_path = try parse_zon_string(arena_allocator, ast, field_index);
        } else if (std.mem.eql(u8, field_name, "lazy")) {
            if (ast.nodes.items(.tag)[field_index] != .identifier) {
                return error.InvalidZon;
            }

            const raw_slice = ast.tokenSlice(ast.nodes.items(.main_token)[field_index]);
            output_lazy = blk: {
                if (std.mem.eql(u8, raw_slice, "true")) {
                    break :blk .true;
                } else if (std.mem.eql(u8, raw_slice, "false")) {
                    break :blk .false;
                } else if (std.mem.eql(u8, raw_slice, "null")) {
                    break :blk .null;
                }

                return error.InvalidZon;
            };
        } else {
            return error.InvalidZonField;
        }
    }

    return
        if (output_hash != null and output_url != null and output_path == null)
            .{
                .kind = .{
                    .url = .{
                        .hash = output_hash.?,
                        .url = output_url.?,
                    },
                },
                .lazy = output_lazy,
                .name = try arena_allocator.dupe(u8, dependency_name),
            }
        else if (output_hash == null and output_url == null and output_path != null)
            .{
                .kind = .{ .path = output_path.? },
                .lazy = output_lazy,
                .name = try arena_allocator.dupe(u8, dependency_name),
            }
        else
            error.InvalidZon;
}

fn parse_zon_dependencies(
    temp_allocator: std.mem.Allocator,
    arena_allocator: std.mem.Allocator,
    ast: std.zig.Ast,
    index: std.zig.Ast.Node.Index,
) ![]const Dependency {
    var zon_buf: [2]std.zig.Ast.Node.Index = undefined;
    const all_dependencies_struct = ast.fullStructInit(&zon_buf, index)
        orelse return error.InvalidZon;

    var output_dependencies = try std.ArrayList(Dependency).initCapacity(
        arena_allocator,
        all_dependencies_struct.ast.fields.len,
    );
    defer output_dependencies.deinit();

    for (all_dependencies_struct.ast.fields) |field_index| {
        const name_token_index = ast.firstToken(field_index) - 2;
        const raw_name = ast.tokenSlice(name_token_index);

        var temp_name_buffer = std.ArrayList(u8).init(temp_allocator);
        defer temp_name_buffer.deinit();
        const dependency_name = blk: {
            if (raw_name.len > 0 and raw_name[0] == '@') {
                switch (try std.zig.string_literal.parseWrite(temp_name_buffer.writer(), raw_name[1..])) {
                    .success => break :blk temp_name_buffer.items,
                    .failure => return error.InvalidStringLiteral,
                }
            }

            break :blk raw_name;
        };

        try output_dependencies.append(
            try parse_zon_dependency(
                temp_allocator,
                arena_allocator,
                dependency_name,
                ast,
                field_index,
            )
        );
    }

    return output_dependencies.toOwnedSlice();
}

pub fn parse_zon_from_bytes(
    allocator: std.mem.Allocator,
    bytes: [:0]const u8,
) !std.json.Parsed(BuildZigZon) {
    var ast = try std.zig.Ast.parse(allocator, bytes, .zon);
    defer ast.deinit(allocator);
    if (ast.errors.len > 0) {
        return error.InvalidZon;
    }

    var zon_buf: [2]std.zig.Ast.Node.Index = undefined;
    const zon_struct = ast.fullStructInit(&zon_buf, ast.nodes.items(.data)[0].lhs)
        orelse return error.InvalidZon;

    var parsed = std.json.Parsed(BuildZigZon){
        .arena = try allocator.create(std.heap.ArenaAllocator),
        .value = undefined,
    };
    errdefer allocator.destroy(parsed.arena);
    parsed.arena.* = std.heap.ArenaAllocator.init(allocator);
    errdefer parsed.arena.deinit();

    var output_name: ?[]const u8 = null;
    var output_version: ?std.SemanticVersion = null;
    var output_minimum_zig_version: ?std.SemanticVersion = null;
    var output_dependencies: ?[]const Dependency = null;
    var output_paths: ?[]const []const u8 = null;

    for (zon_struct.ast.fields) |field_index| {
        const name_token_index = ast.firstToken(field_index) - 2;
        const raw_name = ast.tokenSlice(name_token_index);

        var temp_name_buffer = std.ArrayList(u8).init(allocator);
        defer temp_name_buffer.deinit();
        const field_name = blk: {
            if (raw_name.len > 0 and raw_name[0] == '@') {
                switch (try std.zig.string_literal.parseWrite(temp_name_buffer.writer(), raw_name[1..])) {
                    .success => break :blk temp_name_buffer.items,
                    .failure => return error.InvalidStringLiteral,
                }
            }

            break :blk raw_name;
        };

        if (std.mem.eql(u8, field_name, "name")) {
            output_name = try parse_zon_string(parsed.arena.allocator(), ast, field_index);
        } else if (std.mem.eql(u8, field_name, "version")) {
            const version = try parse_zon_string(allocator, ast, field_index);
            defer allocator.free(version);
            output_version = try std.SemanticVersion.parse(version);
        } else if (std.mem.eql(u8, field_name, "minimum_zig_version")) {
            const version = try parse_zon_string(allocator, ast, field_index);
            defer allocator.free(version);
            output_minimum_zig_version = try std.SemanticVersion.parse(version);
        } else if (std.mem.eql(u8, field_name, "dependencies")) {
            output_dependencies = try parse_zon_dependencies(allocator, parsed.arena.allocator(), ast, field_index);
        } else if (std.mem.eql(u8, field_name, "paths")) {
            output_paths = try parse_zon_string_array(parsed.arena.allocator(), ast, field_index);
        } else {
            return error.InvalidZonField;
        }
    }

    parsed.value = .{
        .name = output_name.?,
        .version = output_version.?,
        .minimum_zig_version = output_minimum_zig_version,
        .dependencies = output_dependencies.?,
        .paths = output_paths.?,
    };
    return parsed;
}

pub fn parse_zon_from_file_path(
    allocator: std.mem.Allocator,
    file_path: []const u8,
) !std.json.Parsed(BuildZigZon) {
    const zon_contents: [:0]const u8 = blk: {
        const file = try std.fs.openFileAbsolute(file_path, .{});
        defer file.close();

        var output_buffer = try std.ArrayList(u8).initCapacity(allocator, try file.getEndPos());
        defer output_buffer.deinit();

        try file.reader().readAllArrayList(&output_buffer, std.math.maxInt(usize));
        break :blk try output_buffer.toOwnedSliceSentinel(0);
    };
    defer allocator.free(zon_contents);

    return parse_zon_from_bytes(allocator, zon_contents);
}
