const std = @import("std");

const json_types = @import("json_types.zig");

const yazap = @import("yazap");


fn write_instruction_enum(
    writer: anytype,
    prefix: []const u8,
    name: []const u8,
    instructions: []const json_types.Instruction,
) !void {
    try writer.print("enum {s}{s} {{\n", .{ prefix, name });
    for (instructions) |instruction| {
        try writer.print("    {s}{s} = {d},\n", .{ prefix, instruction.opname, instruction.opcode });
    }
    _ = try writer.print("    {s}{s}Max = 0x7ffffff\n}};\n\n\n", .{ prefix, name, });
}

fn write_enumerant_enum(
    writer: anytype,
    prefix: []const u8,
    name: []const u8,
    enumerants: []const json_types.Enumerant,
) !void {
    try writer.print("enum {s}{s} {{\n", .{ prefix, name });
    for (enumerants) |enumerant| {
        try writer.print("    {s}{s} = {s},\n", .{ prefix, enumerant.enumerant, enumerant.value.?.raw });
    }
    _ = try writer.print("    {s}{s}Max = 0x7ffffff\n}};\n\n", .{ prefix, name, });
}

pub fn main() !u8 {
    var gpa: std.heap.GeneralPurposeAllocator(.{}) = .{};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    var app = yazap.App.init(
        allocator,
        "generate_language_headers",
        "SPIRV-Tools/generate_language_headers.py ported to zig",
    );
    defer app.deinit();
    var root_cmd = app.rootCommand();
    try root_cmd.addArg(yazap.Arg.singleValueOption("extinst-grammar", null, null));
    try root_cmd.addArg(yazap.Arg.singleValueOption("extinst-output-path", null, null));

    const matches = try app.parseProcess();
    if (
        !matches.containsArgs() or
        !(matches.containsArg("extinst-grammar") and matches.containsArg("extinst-output-path"))
    ) {
        try app.displayHelp();
        return 1;
    }
    const grammar_path_raw = matches.getSingleValue("extinst-grammar").?;
    const output_file_path = matches.getSingleValue("extinst-output-path").?;

    const output_dir_path = std.fs.path.dirname(output_file_path)
        orelse return error.InvalidOutputPath;
    const output_filename = std.fs.path.basename(output_file_path);

    // the current working directory does not need to be closed manually like
    // other directories, process exit will clean it up.
    const cwd = std.fs.cwd();
    const new_file_contents = blk: {
        const grammar_name = std.fs.path.stem(output_filename);

        const grammar_file = try cwd.openFile(grammar_path_raw, .{});
        defer grammar_file.close();

        var json_reader = std.json.reader(allocator, grammar_file.reader());
        defer json_reader.deinit();
        const result = try std.json.parseFromTokenSource(
            json_types.Grammar,
            allocator,
            &json_reader,
            .{
                .allocate = .alloc_always,
                .ignore_unknown_fields = true,
            },
        );
        defer result.deinit();

        var output_buffer = std.ArrayList(u8).init(allocator);
        defer output_buffer.deinit();
        const output_writer = output_buffer.writer();

        for (result.value.copyright.?) |line| {
            try output_writer.print("// {s}\n", .{ line });
        }

        try output_writer.print(
            "\n#ifndef SPIRV_EXTINST_{s}_H_\n#define SPIRV_EXTINST_{s}_H_\n",
            .{
                grammar_name,
                grammar_name,
            },
        );

        _ = try output_writer.write("\n#ifdef __cplusplus\nextern \"C\" {\n#endif\n\n");
        try output_writer.print(
            "enum {{ {[prefix]s}{[variable]s} = {[value]d}, {[prefix]s}Version_BitWidthPadding = 0x7fffffff }};\n",
            .{
                .prefix = grammar_name,
                .variable = "Version",
                .value = result.value.version.?,
            },
        );
        try output_writer.print(
            "enum {{ {[prefix]s}{[variable]s} = {[value]d}, {[prefix]s}Revision_BitWidthPadding = 0x7fffffff }};\n\n",
            .{
                .prefix = grammar_name,
                .variable = "Revision",
                .value = result.value.revision,
            },
        );

        try write_instruction_enum(output_writer, grammar_name, "Instructions", result.value.instructions);

        for (result.value.operand_kinds.?) |op_kind| {
            try write_enumerant_enum(output_writer, grammar_name, op_kind.kind, op_kind.enumerants.?);
        }

        try output_writer.print(
            "\n#ifdef __cplusplus\n}}\n#endif\n\n#endif // SPIRV_EXTINST_{s}_H_",
            .{
                grammar_name,
            },
        );

        break :blk try output_buffer.toOwnedSlice();
    };
    defer allocator.free(new_file_contents);

    // cwd.openFile accepts relative and absolute paths
    const output_file = cwd.openFile(output_file_path, .{ .mode = .write_only })
        catch |err| switch (err) {
            error.FileNotFound => blk: {
                var output_dir = try cwd.makeOpenPath(output_dir_path, .{});
                defer output_dir.close();
                break :blk try output_dir.createFile(output_filename, .{ .mode = 0o644 });
            },
            else => return err,
        };
    defer output_file.close();

    try output_file.writeAll(new_file_contents);
    try output_file.setEndPos(new_file_contents.len);
    return 0;
}
