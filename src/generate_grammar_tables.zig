const std = @import("std");

const json_types = @import("json_types.zig");

const yazap = @import("yazap");


const StringListContext = struct {
    pub fn hash(_: @This(), strings: []const []const u8) u32 {
        var hasher = std.crypto.hash.Md5.init(.{});
        for (strings) |string| {
            hasher.update(string);
        }
        var out_buf: [std.crypto.hash.Md5.digest_length]u8 = undefined;
        hasher.final(&out_buf);
        return @truncate(@as(u128, @bitCast(out_buf)));
    }
    pub fn eql(_: @This(), lhs: []const []const u8, rhs: []const []const u8, _: usize) bool {
        if (lhs.len != rhs.len) return false;

        for (lhs, rhs) |l, r| {
            if (!std.array_hash_map.eqlString(l, r)) {
                return false;
            }
        }

        return true;
    }
};
const StringListSet = std.ArrayHashMap([]const []const u8, void, StringListContext, true);
const StringListSetSortContext = struct {
    string_lists: [][]const []const u8,

    pub fn lessThan(ctx: @This(), lhs_index: usize, rhs_index: usize) bool {
        const lhs = ctx.string_lists[lhs_index];
        const rhs = ctx.string_lists[rhs_index];

        var i: usize = 0;
        while (true) : (i += 1) {
            if (i >= lhs.len or i >= rhs.len) {
                return lhs.len < rhs.len;
            } else if (std.array_hash_map.eqlString(lhs[i], rhs[i])) {
                continue;
            }

            return std.mem.lessThan(u8, lhs[i], rhs[i]);
        }
    }
};
const LanguageMode = enum {
    c,
    @"c++",
};

const EXTENSIONS_FROM_SPIRV_REGISTRY_AND_NOT_FROM_GRAMMARS: []const []const u8 = &.{
    "SPV_AMD_gcn_shader",
    "SPV_AMD_gpu_shader_half_float",
    "SPV_AMD_gpu_shader_int16",
    "SPV_AMD_shader_trinary_minmax",
    "SPV_KHR_non_semantic_info",
};
const PYGEN_VARIABLE_PREFIX: []const u8 = "pygen_variable";

fn parse_grammar_json(
    allocator: std.mem.Allocator,
    cwd: std.fs.Dir,
    path: []const u8,
) !std.json.Parsed(json_types.Grammar) {
    const grammar_file = try cwd.openFile(path, .{});
    defer grammar_file.close();

    var json_reader = std.json.reader(allocator, grammar_file.reader());
    defer json_reader.deinit();
    return std.json.parseFromTokenSource(
        json_types.Grammar,
        allocator,
        &json_reader,
        .{ .allocate = .alloc_always },
    );
}

fn get_grammar_set_name(allocator: std.mem.Allocator, path: []const u8) ![]const u8 {
    const basename = std.fs.path.basename(path);
    const starter = "extinst.";
    if (std.mem.indexOf(u8, basename, starter)) |found1| {
        const result1 = basename[found1 + starter.len..basename.len];
        const ender = ".grammar.json";
        if (std.mem.indexOf(u8, result1, ender)) |found2| {
            const result2 = result1[0..found2];
            var buffer = std.ArrayList(u8).init(allocator);
            defer buffer.deinit();

            for (result2) |c| {
                switch (c) {
                    '.', '-' => try buffer.append('_'),
                    else => |fwd| try buffer.append(fwd)
                }
            }

            return buffer.toOwnedSlice();
        }
    }

    return error.InvalidName;
}

fn collect_instructions_capabilities_set(
    capabilities_set: *StringListSet,
    instructions: []const json_types.Instruction,
) !void {
    for (instructions) |instruction| {
        if (instruction.capabilities) |capabilities| {
            try capabilities_set.put(capabilities, {});
        }
    }

    capabilities_set.unmanaged.sortUnstable(StringListSetSortContext{ .string_lists = capabilities_set.keys() });
}

fn write_single_capabilities_entry(
    output_buffer: *std.ArrayList(u8),
    capabilities: []const []const u8,
    language_mode: LanguageMode,
) !void {
    try output_buffer.appendSlice("static const ");
    try output_buffer.appendSlice(
        switch (language_mode) {
            .c => "SpvCapability ",
            .@"c++" => "spv::Capability ",
        }
    );
    try output_buffer.appendSlice(PYGEN_VARIABLE_PREFIX);
    try output_buffer.appendSlice("_caps_");
    for (capabilities) |capability| {
        try output_buffer.appendSlice(capability);
    }
    try output_buffer.appendSlice("[] = {");

    var first_iter_complete = false;
    for (capabilities) |capability| {
        if (first_iter_complete) {
            try output_buffer.appendSlice(", ");
        }
        first_iter_complete = true;

        try output_buffer.appendSlice(
            switch (language_mode) {
                .c => "SpvCapability",
                .@"c++" => "spv::Capability::",
            }
        );
        try output_buffer.appendSlice(capability);
    }

    try output_buffer.appendSlice("};\n");
}

fn collect_instructions_extensions_set(
    extensions_set: *StringListSet,
    instructions: []const json_types.Instruction,
) !void {
    for (instructions) |instruction| {
        if (instruction.extensions) |extensions| {
            try extensions_set.put(extensions, {});
        }
    }

    extensions_set.unmanaged.sortUnstable(StringListSetSortContext{ .string_lists = extensions_set.keys() });
}

fn write_single_extensions_entry(
    output_buffer: *std.ArrayList(u8),
    extensions: []const []const u8,
) !void {
    try output_buffer.appendSlice("static const spvtools::Extension ");
    try output_buffer.appendSlice(PYGEN_VARIABLE_PREFIX);
    try output_buffer.appendSlice("_exts_");
    for (extensions) |extension| {
        try output_buffer.appendSlice(extension);
    }
    try output_buffer.appendSlice("[] = {");

    var first_iter_complete = false;
    for (extensions) |extension| {
        if (first_iter_complete) {
            try output_buffer.appendSlice(", ");
        }
        first_iter_complete = true;

        try output_buffer.appendSlice("spvtools::Extension::k");
        try output_buffer.appendSlice(extension);
    }

    try output_buffer.appendSlice("};\n");
}

fn write_converted_operand_kind(
    output_buffer: *std.ArrayList(u8),
    raw_kind: []const u8,
    raw_quantifier: ?[]const u8,
) !void {
    const kind_replacements = std.StaticStringMap([]const u8).initComptime(.{
        .{ "IdResultType", "TypeId" },
        .{ "IdResult", "ResultId" },
        .{ "IdMemorySemantics", "MemorySemanticsId" },
        .{ "MemorySemantics", "MemorySemanticsId" },
        .{ "IdScope", "ScopeId" },
        .{ "Scope", "ScopeId" },
        .{ "IdRef", "Id" },
        .{ "ImageOperands", "Image" },
        .{ "Dim", "Dimensionality" },
        .{ "ImageFormat", "SamplerImageFormat" },
        .{ "KernelEnqueueFlags", "KernelEnqFlags" },
        .{ "LiteralExtInstInteger", "ExtensionInstructionNumber" },
        .{ "LiteralSpecConstantOpInteger", "SpecConstantOpNumber" },
        .{ "LiteralContextDependentNumber", "TypedLiteralNumber" },
        .{ "PairLiteralIntegerIdRef", "LiteralIntegerId" },
        .{ "PairIdRefLiteralInteger", "IdLiteralInteger" },
        .{ "PairIdRefIdRef", "Id" },
        .{ "FPRoundingMode", "FpRoundingMode" },
        .{ "FPFastMathMode", "FpFastMathMode" },
    });

    const quantifier_replacements = std.StaticStringMap([]const u8).initComptime(.{
        .{ "?", "OPTIONAL_" },
        .{ "*", "VARIABLE_" },
    });

    try output_buffer.appendSlice("SPV_OPERAND_TYPE_");
    if (raw_quantifier) |quantifier| {
        if (quantifier_replacements.get(quantifier)) |replacement| {
            try output_buffer.appendSlice(replacement);
        }
    }

    const kind = kind_replacements.get(raw_kind) orelse raw_kind;
    var last_char_was_lowercase = false;
    for (kind) |char| {
        if (last_char_was_lowercase and std.ascii.isUpper(char)) {
            try output_buffer.append('_');
        }

        try output_buffer.append(std.ascii.toUpper(char));
        last_char_was_lowercase = std.ascii.isLower(char);
    }
}

fn write_min_version(
    output_buffer: *std.ArrayList(u8),
    version: ?[]const u8,
) !void {
    if (version) |min_version| {
        if (std.mem.eql(u8, min_version, "None")) {
            try output_buffer.appendSlice("0xffffffffu");
        } else {
            try output_buffer.appendSlice("SPV_SPIRV_VERSION_WORD(");
            for (min_version) |char| {
                switch (char) {
                    '.' => try output_buffer.append(','),
                    else => try output_buffer.append(char),
                }
            }
            try output_buffer.appendSlice(")");
        }
    } else {
        try output_buffer.appendSlice("SPV_SPIRV_VERSION_WORD(1, 0)");
    }
}

fn write_max_version(
    output_buffer: *std.ArrayList(u8),
    version: ?[]const u8,
) !void {
    if (version) |max_version| {
        try output_buffer.appendSlice("SPV_SPIRV_VERSION_WORD(");
        for (max_version) |char| {
            switch (char) {
                '.' => try output_buffer.append(','),
                else => try output_buffer.append(char),
            }
        }
        try output_buffer.appendSlice(")");
    } else {
        try output_buffer.appendSlice("0xffffffffu");
    }
}

fn write_single_instruction(
    output_buffer: *std.ArrayList(u8),
    instruction: json_types.Instruction,
    language_mode: LanguageMode,
) !void {
    if (!std.mem.startsWith(u8, instruction.opname, "Op")) {
        return error.InvalidInstruction;
    }

    // Remove the "Op" prefix
    const trimmed_opname = instruction.opname[2..];
    const capabilities: []const []const u8 = instruction.capabilities orelse &.{};
    const extensions: []const []const u8 = instruction.extensions orelse &.{};
    const operands: []const json_types.Operand = blk: {
        if (instruction.operands) |operands| {
            if (std.mem.eql(u8, trimmed_opname, "ExtInst")) {
                var fba_buf: [256]u8 = undefined;
                var fba = std.heap.FixedBufferAllocator.init(&fba_buf);
                var temp_operand_buf = std.ArrayList(u8).init(fba.allocator());
                defer temp_operand_buf.deinit();

                const operand = operands[operands.len - 1];
                try write_converted_operand_kind(&temp_operand_buf, operand.kind, operand.quantifier);

                if (std.mem.eql(u8, temp_operand_buf.items, "SPV_OPERAND_TYPE_VARIABLE_ID")) {
                    // Fix an instruction's syntax, adjusting for differences between the
                    // officially released grammar and how SPIRV-Tools uses the grammar.
                    //
                    // Fixes:
                    //    - ExtInst should not end with SPV_OPERAND_VARIABLE_ID.
                    //    https://github.com/KhronosGroup/SPIRV-Tools/issues/233
                    break :blk operands[0..operands.len - 1];
                }
            }

            break :blk operands;
        }

        break :blk &.{};
    };

    // line start and quoted name start
    try output_buffer.appendSlice("  {\"");
    try output_buffer.appendSlice(trimmed_opname);
    // end quote
    try output_buffer.appendSlice("\", ");
    // fully qualified opname
    try output_buffer.appendSlice(
        switch (language_mode) {
            .c => "SpvOp",
            .@"c++" => "spv::Op::Op",
        }
    );
    try output_buffer.appendSlice(trimmed_opname);

    var writer = output_buffer.writer();
    // num_caps
    try writer.print(", {d}, ", .{ capabilities.len });

    // caps_mask
    if (capabilities.len == 0) {
        try output_buffer.appendSlice("nullptr, ");
    } else {
        try output_buffer.appendSlice(PYGEN_VARIABLE_PREFIX);
        try output_buffer.appendSlice("_caps_");
        for (capabilities) |capability| {
            try output_buffer.appendSlice(capability);
        }
        try output_buffer.appendSlice(", ");
    }

    // num_operands
    try writer.print("{d}, ", .{ operands.len });

    // operands
    {
        try output_buffer.appendSlice("{");
        var first_iter_complete = false;
        for (operands) |operand| {
            if (first_iter_complete) {
                try output_buffer.appendSlice(", ");
            }
            first_iter_complete = true;

            try write_converted_operand_kind(output_buffer, operand.kind, operand.quantifier);
        }
        try output_buffer.appendSlice("}, ");
    }

    // def_result_id, ref_type_id, and num_exts
    {
        var contains_id_result = false;
        var contains_id_result_type = false;
        for (operands) |operand| {
            contains_id_result = contains_id_result or std.mem.eql(u8, operand.kind, "IdResult");
            contains_id_result_type = contains_id_result_type or std.mem.eql(u8, operand.kind, "IdResultType");
        }

        try writer.print("{d}, {d}, {d}, ", .{
            @intFromBool(contains_id_result),
            @intFromBool(contains_id_result_type),
            extensions.len,
        });
    }

    // exts
    if (extensions.len > 0) {
        try output_buffer.appendSlice(PYGEN_VARIABLE_PREFIX);
        try output_buffer.appendSlice("_exts_");
        for (extensions) |extension| {
            try output_buffer.appendSlice(extension);
        }
    } else {
        try output_buffer.appendSlice("nullptr");
    }
    try output_buffer.appendSlice(", ");

    // min_version
    try write_min_version(output_buffer, instruction.version);
    try output_buffer.appendSlice(", ");

    // max_version
    try write_max_version(output_buffer, instruction.lastVersion);
    try output_buffer.appendSlice("}");
}

fn generate_instruction_table(
    allocator: std.mem.Allocator,
    output_buffer: *std.ArrayList(u8),
    instructions: []const json_types.Instruction,
    language_mode: LanguageMode,
) !void {
    {
        var capabilities_set = StringListSet.init(allocator);
        defer capabilities_set.deinit();
        try collect_instructions_capabilities_set(&capabilities_set, instructions);

        for (capabilities_set.keys()) |capabilities| {
            try write_single_capabilities_entry(output_buffer, capabilities, language_mode);
        }
    }

    try output_buffer.appendSlice("\n");

    {
        var extensions_set = StringListSet.init(allocator);
        defer extensions_set.deinit();
        try collect_instructions_extensions_set(&extensions_set, instructions);

        for (extensions_set.keys()) |extensions| {
            try write_single_extensions_entry(output_buffer, extensions);
        }
    }

    try output_buffer.appendSlice("\nstatic const spv_opcode_desc_t kOpcodeTableEntries[] = {\n");
    {
        var first_iter_complete = false;
        for (instructions) |instruction| {
            if (first_iter_complete) {
                try output_buffer.appendSlice(",\n");
            }
            first_iter_complete = true;

            try write_single_instruction(output_buffer, instruction, language_mode);
        }
    }
    try output_buffer.appendSlice("\n};");
}

fn precondition_operand_kinds(
    allocator: std.mem.Allocator,
    core_operand_kinds: []const json_types.OperandKind,
    debuginfo_operand_kinds: []const json_types.OperandKind,
    cldebuginfo100_operand_kinds: []const json_types.OperandKind,
) !std.json.Parsed([]const json_types.OperandKind) {
    var exts_arena = std.heap.ArenaAllocator.init(allocator);
    defer exts_arena.deinit();
    var exts = std.StringArrayHashMap(std.StringArrayHashMap(void)).init(exts_arena.allocator());
    defer exts.deinit();

    const all_opkinds = .{ core_operand_kinds, debuginfo_operand_kinds, cldebuginfo100_operand_kinds };
    inline for (all_opkinds) |operand_kinds| {
        for (operand_kinds) |operand_kind| {
            if (operand_kind.enumerants) |enumerants| {
                for (enumerants) |enumerant| {
                    if (enumerant.value) |value| {
                        const key = try std.fmt.allocPrint(
                            exts_arena.allocator(),
                            "{s}.{s}",
                            .{
                                operand_kind.kind,
                                value.raw,
                            },
                        );

                        const gop = try exts.getOrPut(key);
                        if (!gop.found_existing) {
                            gop.value_ptr.* = std.StringArrayHashMap(void).init(exts_arena.allocator());
                        }

                        if (enumerant.extensions) |enum_exts| {
                            for (enum_exts) |extension| {
                                try gop.value_ptr.put(extension, {});
                            }
                        }
                    }
                }
            }
        }
    }

    const SortContext = struct {
        sets: [][]const u8,

        pub fn lessThan(ctx: @This(), lhs_index: usize, rhs_index: usize) bool {
            return std.mem.lessThan(u8, ctx.sets[lhs_index], ctx.sets[rhs_index]);
        }
    };
    for (exts.values()) |*extension_set| {
        extension_set.unmanaged.sortUnstable(SortContext{ .sets = extension_set.keys() });
    }

    var container: std.json.Parsed([]const json_types.OperandKind) = .{
        .arena = try allocator.create(std.heap.ArenaAllocator),
        .value = undefined,
    };
    errdefer allocator.destroy(container.arena);
    container.arena.* = std.heap.ArenaAllocator.init(allocator);
    var preconditioned_operand_kinds = std.ArrayList(json_types.OperandKind).init(container.arena.allocator());
    defer preconditioned_operand_kinds.deinit();

    var key_buf: [1024]u8 = undefined;
    const opkind_prefixes: []const ?[]const u8 = &.{ null, null, "CLDEBUG100_" };
    inline for (all_opkinds, opkind_prefixes) |operand_kinds, maybe_prefix| {
        for (operand_kinds) |operand_kind| {
            if (
                !(
                    std.mem.eql(u8, operand_kind.category, "BitEnum") or
                    std.mem.eql(u8, operand_kind.category, "ValueEnum")
                )
            ) {
                continue;
            }

            const prefixed_kind: []const u8 =
                if (maybe_prefix) |prefix|
                    try std.fmt.allocPrint(
                        container.arena.allocator(),
                        "{s}{s}",
                        .{
                            prefix,
                            operand_kind.kind,
                        },
                    )
                else
                    operand_kind.kind;

            if (operand_kind.enumerants) |enumerants| {
                var enumerant_buffer = std.ArrayList(json_types.Enumerant).init(container.arena.allocator());
                defer enumerant_buffer.deinit();

                for (enumerants) |enumerant| {
                    if (enumerant.value) |value| {
                        const key = try std.fmt.bufPrint(
                            &key_buf,
                            "{s}.{s}",
                            .{
                                operand_kind.kind,
                                value.raw,
                            },
                        );

                        if (exts.get(key)) |exts_set| {
                            if (exts_set.count() > 0) {
                                try enumerant_buffer.append(.{
                                    .capabilities = enumerant.capabilities,
                                    .enumerant = enumerant.enumerant,
                                    .extensions = try container.arena.allocator().dupe([]const u8, exts_set.keys()),
                                    .lastVersion = enumerant.lastVersion,
                                    .parameters = enumerant.parameters,
                                    .value = enumerant.value,
                                    .version = enumerant.version,
                                });
                                continue;
                            }
                        }
                    }

                    try enumerant_buffer.append(enumerant);
                }

                try preconditioned_operand_kinds.append(operand_kind.dupe(prefixed_kind, try enumerant_buffer.toOwnedSlice()));
                continue;
            }

            try preconditioned_operand_kinds.append(operand_kind.dupe(prefixed_kind, null));
        }
    }

    container.value = try preconditioned_operand_kinds.toOwnedSlice();
    return container;
}

fn write_single_enum_operand_kind_entry(
    output_buffer: *std.ArrayList(u8),
    op_to_extension_map: std.AutoArrayHashMap(u64, std.StringArrayHashMap(void)),
    enumerant: json_types.Enumerant,
) !void {
    const enumerant_name = enumerant.enumerant;
    const capabilities: []const []const u8 = enumerant.capabilities orelse &.{};
    const extensions: []const []const u8 =
        if (op_to_extension_map.get(enumerant.value.?.num)) |exts|
            exts.keys()
        else
            &.{};

    var writer = output_buffer.writer();
    // enumerant
    try writer.print("  {{\"{}\", ", .{ std.zig.fmtEscapes(enumerant_name) });

    // value
    try output_buffer.appendSlice(enumerant.value.?.raw);
    try output_buffer.appendSlice(", ");

    // num_caps
    try writer.print("{d}, ", .{ capabilities.len });

    // caps
    if (capabilities.len == 0) {
        try output_buffer.appendSlice("nullptr, ");
    } else {
        try output_buffer.appendSlice(PYGEN_VARIABLE_PREFIX);
        try output_buffer.appendSlice("_caps_");
        for (capabilities) |capability| {
            try output_buffer.appendSlice(capability);
        }
        try output_buffer.appendSlice(", ");
    }

    // num_exts
    try writer.print("{d}, ", .{ extensions.len });

    // exts
    if (extensions.len > 0) {
        try output_buffer.appendSlice(PYGEN_VARIABLE_PREFIX);
        try output_buffer.appendSlice("_exts_");
        for (extensions) |extension| {
            try output_buffer.appendSlice(extension);
        }
    } else {
        try output_buffer.appendSlice("nullptr");
    }
    try output_buffer.appendSlice(", ");

    // parameters
    {
        try output_buffer.appendSlice("{");
        if (enumerant.parameters) |parameters| {
            var first_iter_complete = false;
            for (parameters) |parameter| {
                if (first_iter_complete) {
                    try output_buffer.appendSlice(", ");
                }
                first_iter_complete = true;

                try write_converted_operand_kind(
                    output_buffer,
                    parameter.kind,
                    null, // for some reason the python script strips these
                );
            }
        }
        try output_buffer.appendSlice("}, ");
    }

    // min_version
    try write_min_version(output_buffer, enumerant.version);
    try output_buffer.appendSlice(", ");

    // max_version
    try write_max_version(output_buffer, enumerant.lastVersion);
    try output_buffer.appendSlice("}");

}

fn generate_operand_kind_table(
    allocator: std.mem.Allocator,
    output_buffer: *std.ArrayList(u8),
    core_operand_kinds: []const json_types.OperandKind,
    debuginfo_operand_kinds: []const json_types.OperandKind,
    cldebuginfo100_operand_kinds: []const json_types.OperandKind,
) !void {
    var preconditioned_operand_kinds = try precondition_operand_kinds(
        allocator,
        core_operand_kinds,
        debuginfo_operand_kinds,
        cldebuginfo100_operand_kinds,
    );
    defer preconditioned_operand_kinds.deinit();

    {
        var capabilities_set = StringListSet.init(allocator);
        defer capabilities_set.deinit();

        for (preconditioned_operand_kinds.value) |operand_kind| {
            if (operand_kind.enumerants) |enumerants| {
                for (enumerants) |enumerant| {
                    if (enumerant.capabilities) |capabilities| {
                        try capabilities_set.put(capabilities, {});
                    }
                }
            }
        }

        capabilities_set.unmanaged.sortUnstable(StringListSetSortContext{ .string_lists = capabilities_set.keys() });
        for (capabilities_set.keys()) |capabilities| {
            try write_single_capabilities_entry(output_buffer, capabilities, .@"c++");
        }
    }
    try output_buffer.append('\n');

    {
        var extensions_set = StringListSet.init(allocator);
        defer extensions_set.deinit();

        for (preconditioned_operand_kinds.value) |operand_kind| {
            if (operand_kind.enumerants) |enumerants| {
                for (enumerants) |enumerant| {
                    if (enumerant.extensions) |extensions| {
                        try extensions_set.put(extensions, {});
                    }
                }
            }
        }

        extensions_set.unmanaged.sortUnstable(StringListSetSortContext{ .string_lists = extensions_set.keys() });
        for (extensions_set.keys()) |extensions| {
            try write_single_extensions_entry(output_buffer, extensions);
        }
    }
    try output_buffer.append('\n');

    for (preconditioned_operand_kinds.value) |operand_kind| {
        var enumerants_buffer = std.ArrayList(json_types.Enumerant).init(allocator);
        defer enumerants_buffer.deinit();
        try enumerants_buffer.appendSlice(operand_kind.enumerants.?);

        std.mem.sortUnstable(
            json_types.Enumerant,
            enumerants_buffer.items,
            {},
            struct {
                fn lessThan(_: void, lhs: json_types.Enumerant, rhs: json_types.Enumerant) bool {
                    return lhs.value.?.num < rhs.value.?.num;
                }
            }.lessThan,
        );

        var map_arena = std.heap.ArenaAllocator.init(allocator);
        defer map_arena.deinit();
        var op_to_extension_map = std.AutoArrayHashMap(u64, std.StringArrayHashMap(void)).init(map_arena.allocator());
        defer op_to_extension_map.deinit();
        for (enumerants_buffer.items) |enumerant| {
            const gop = try op_to_extension_map.getOrPut(enumerant.value.?.num);
            if (!gop.found_existing) {
                gop.value_ptr.* = std.StringArrayHashMap(void).init(map_arena.allocator());
            }

            if (enumerant.extensions) |extensions| {
                for (extensions) |extension| {
                    try gop.value_ptr.put(extension, {});
                }
            }
        }

        try output_buffer.appendSlice("static const spv_operand_desc_t ");
        try output_buffer.appendSlice(PYGEN_VARIABLE_PREFIX);
        try output_buffer.appendSlice("_");
        try output_buffer.appendSlice(operand_kind.kind);
        try output_buffer.appendSlice("Entries[] = {\n");

        {
            var first_iter_complete = false;
            for (enumerants_buffer.items) |enumerant| {
                if (first_iter_complete) {
                    try output_buffer.appendSlice(",\n");
                }
                first_iter_complete = true;

                try write_single_enum_operand_kind_entry(output_buffer, op_to_extension_map, enumerant);
            }
        }

        try output_buffer.appendSlice("\n};\n\n");
    }

    const OpTableEntry = struct {
        kind: []const u8,
        quantifier: ?[]const u8,
    };
    try output_buffer.appendSlice("static const spv_operand_desc_group_t pygen_variable_OperandInfoTable[] = {\n");
    {
        var op_table_entries = std.ArrayList(OpTableEntry).init(allocator);
        defer op_table_entries.deinit();
        for (preconditioned_operand_kinds.value) |operand_kind| {
            try op_table_entries.append(.{ .kind = operand_kind.kind, .quantifier = null });
        }
        try op_table_entries.append(.{ .kind = "ImageOperands", .quantifier = "?" });
        try op_table_entries.append(.{ .kind = "MemoryAccess", .quantifier = "?" });
        try op_table_entries.append(.{ .kind = "RawAccessChainOperands", .quantifier = "?" });
        try op_table_entries.append(.{ .kind = "AccessQualifier", .quantifier = "?" });
        try op_table_entries.append(.{ .kind = "PackedVectorFormat", .quantifier = "?" });
        try op_table_entries.append(.{ .kind = "CooperativeMatrixOperands", .quantifier = "?" });

        var first_iter_complete = false;
        for (op_table_entries.items) |entry| {
            if (first_iter_complete) {
                try output_buffer.appendSlice(",\n");
            }
            first_iter_complete = true;

            try output_buffer.appendSlice("  {");
            try write_converted_operand_kind(
                output_buffer,
                entry.kind,
                entry.quantifier,
            );
            try output_buffer.appendSlice(", ARRAY_SIZE(");
            try output_buffer.appendSlice(PYGEN_VARIABLE_PREFIX);
            try output_buffer.appendSlice("_");
            try output_buffer.appendSlice(entry.kind);
            try output_buffer.appendSlice("Entries), ");
            try output_buffer.appendSlice(PYGEN_VARIABLE_PREFIX);
            try output_buffer.appendSlice("_");
            try output_buffer.appendSlice(entry.kind);
            try output_buffer.appendSlice("Entries}");
        }
    }
    try output_buffer.appendSlice("\n};");
}

fn write_single_extension_instruction(
    output_buffer: *std.ArrayList(u8),
    instruction: json_types.Instruction,
) !void {
    const caps: []const []const u8 = instruction.capabilities orelse &.{};
    {
        var writer = output_buffer.writer();
        try writer.print(
            "{{\"{s}\", {d}, {d}, ",
            .{
                instruction.opname,
                instruction.opcode,
                caps.len,
            },
        );
    }

    switch (caps.len) {
        0 => try output_buffer.appendSlice("nullptr"),
        else => {
            try output_buffer.appendSlice(PYGEN_VARIABLE_PREFIX);
            try output_buffer.appendSlice("_caps_");
        }
    }

    for (caps) |cap| {
        try output_buffer.appendSlice(cap);
    }
    try output_buffer.appendSlice(", {");

    if (instruction.operands) |ops| {
        var first_iter_complete = false;
        for (ops) |op| {
            if (first_iter_complete) {
                try output_buffer.appendSlice(", ");
            }
            first_iter_complete = true;

            try write_converted_operand_kind(output_buffer, op.kind, op.quantifier);
        }
        try output_buffer.appendSlice(", ");
    }
    try output_buffer.appendSlice("SPV_OPERAND_TYPE_NONE}}");
}

fn generate_extended_instruction_table(
    allocator: std.mem.Allocator,
    output_buffer: *std.ArrayList(u8),
    instructions: []const json_types.Instruction,
    grammar_set_name: []const u8,
    language_mode: LanguageMode,
) !void {
    {
        var capabilities_set = StringListSet.init(allocator);
        defer capabilities_set.deinit();
        try collect_instructions_capabilities_set(&capabilities_set, instructions);

        for (capabilities_set.keys()) |cap| {
            try write_single_capabilities_entry(output_buffer, cap, language_mode);
        }
    }

    if (output_buffer.items.len < 1) {
        try output_buffer.append('\n');
    }

    try output_buffer.appendSlice("\nstatic const spv_ext_inst_desc_t ");
    try output_buffer.appendSlice(grammar_set_name);
    try output_buffer.appendSlice("_entries[] = {\n");
    {
        var first_iter_complete = false;
        for (instructions) |instruction| {
            if (first_iter_complete) {
                try output_buffer.appendSlice(",\n");
            }
            first_iter_complete = true;

            try output_buffer.appendSlice("  ");
            try write_single_extension_instruction(output_buffer, instruction);
        }
    }
    try output_buffer.appendSlice("\n};");
}

fn collect_capabilities(
    capabilities_set: *std.AutoArrayHashMap(u32, []const u8),
    grammar: json_types.Grammar,
) !void {
    for (grammar.operand_kinds.?) |op_kind| {
        if (!std.mem.eql(u8, op_kind.kind, "Capability")) continue;

        for (op_kind.enumerants.?) |capability| {
            const gop = try capabilities_set.getOrPut(try std.fmt.parseInt(u32, capability.value.?.raw, 0));
            if (!gop.found_existing) {
                gop.value_ptr.* = capability.enumerant;
            }
        }
    }
}

fn collect_extensions(
    extensions_buffer: *std.StringArrayHashMap(void),
    grammar: json_types.Grammar,
) !void {
    for (grammar.operand_kinds.?) |opkind| {
        if (opkind.enumerants) |enumerants| {
            for (enumerants) |enumerant| {
                if (enumerant.extensions) |extensions| {
                    for (extensions) |ext| {
                        try extensions_buffer.put(ext, {});
                    }
                }
            }
        }
    }

    for (grammar.instructions) |instruction| {
        if (instruction.extensions) |extensions| {
            for (extensions) |ext| {
                try extensions_buffer.put(ext, {});
            }
        }
    }
}

fn generate_extension_to_string_mapping(output_buffer: *std.ArrayList(u8), extensions: []const []const u8) !void {
    try output_buffer.appendSlice(
        "const char* ExtensionToString(Extension extension) {\n" ++
        "  switch (extension) {\n"
    );

    var writer = output_buffer.writer();
    for (extensions) |ext| {
        try writer.print(
            "    case Extension::k{[extension]s}:\n" ++
            "      return \"{[extension]s}\";\n",
            .{ .extension = ext },
        );
    }

    try output_buffer.appendSlice("  }\n\n  return \"\";\n}");
}

fn generate_string_to_extension_mapping(output_buffer: *std.ArrayList(u8), extensions: []const []const u8) !void {
    try output_buffer.appendSlice(
        "    bool GetExtensionFromString(const char* str, Extension* extension) {\n" ++
        "        static const char* known_ext_strs[] = { "
    );

    var writer = output_buffer.writer();

    {
        var first_iter_complete = false;
        for (extensions) |ext| {
            if (first_iter_complete) {
                try output_buffer.appendSlice(", ");
            }
            first_iter_complete = true;

            try writer.print("\"{s}\"", .{ ext });
        }
    }

    try output_buffer.appendSlice(
        " };\n" ++
        "        static const Extension known_ext_ids[] = { "
    );

    {
        var first_iter_complete = false;
        for (extensions) |ext| {
            if (first_iter_complete) {
                try output_buffer.appendSlice(", ");
            }
            first_iter_complete = true;

            try writer.print("Extension::k{s}", .{ ext });
        }
    }

    try output_buffer.appendSlice(
        " };\n" ++
        "        const auto b = std::begin(known_ext_strs);\n" ++
        "        const auto e = std::end(known_ext_strs);\n" ++
        "        const auto found = std::equal_range(\n" ++
        "            b, e, str, [](const char* str1, const char* str2) {\n" ++
        "                return std::strcmp(str1, str2) < 0;\n" ++
        "            });\n" ++
        "        if (found.first == e || found.first == found.second) return false;\n" ++
        "\n" ++
        "        *extension = known_ext_ids[found.first - b];\n" ++
        "        return true;\n" ++
        "    }"
    );
}

fn write_buffer_to_file(cwd: std.fs.Dir, output_file_path: []const u8, buffer: []const u8) !void {
    const output_dir_path = std.fs.path.dirname(output_file_path)
        orelse return error.InvalidOutputPath;
    const output_filename = std.fs.path.basename(output_file_path);

    // cwd.openFile accepts relative and absolute paths
    const output_file = cwd.openFile(output_file_path, .{ .mode = .write_only })
        catch |err| switch (err) {
            error.FileNotFound => blk: {
                var output_dir = try cwd.makeOpenPath(output_dir_path, .{});
                defer output_dir.close();
                break :blk try output_dir.createFile(output_filename, .{ .mode = 0o644 });
            },
            else => return err,
        };
    defer output_file.close();

    try output_file.writeAll(buffer);
    try output_file.setEndPos(buffer.len);
}

pub fn main() !u8 {
    var gpa: std.heap.GeneralPurposeAllocator(.{}) = .{};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    var app = yazap.App.init(
        allocator,
        "generate_grammar_enums",
        "SPIRV-Tools/generate_grammar_tables.py ported to zig",
    );
    defer app.deinit();
    var root_cmd = app.rootCommand();
    try root_cmd.addArg(yazap.Arg.singleValueOption("spirv-core-grammar", null, null));
    try root_cmd.addArg(yazap.Arg.singleValueOption("extinst-debuginfo-grammar", null, null));
    try root_cmd.addArg(yazap.Arg.singleValueOption("extinst-cldebuginfo100-grammar", null, null));
    try root_cmd.addArg(yazap.Arg.singleValueOption("extinst-glsl-grammar", null, null));
    try root_cmd.addArg(yazap.Arg.singleValueOption("extinst-opencl-grammar", null, null));
    var output_lang_arg = yazap.Arg.singleValueOptionWithValidValues(
        "output-language",
        null,
        null,
        &.{ "", "c", "c++" },
    );
    output_lang_arg.setProperty(.allow_empty_value);
    try root_cmd.addArg(output_lang_arg);

    try root_cmd.addArg(yazap.Arg.singleValueOption("core-insts-output", null, null));
    try root_cmd.addArg(yazap.Arg.singleValueOption("glsl-insts-output", null, null));
    try root_cmd.addArg(yazap.Arg.singleValueOption("opencl-insts-output", null, null));
    try root_cmd.addArg(yazap.Arg.singleValueOption("operand-kinds-output", null, null));
    try root_cmd.addArg(yazap.Arg.singleValueOption("extension-enum-output", null, null));
    try root_cmd.addArg(yazap.Arg.singleValueOption("enum-string-mapping-output", null, null));
    try root_cmd.addArg(yazap.Arg.singleValueOption("extinst-vendor-grammar", null, null));
    try root_cmd.addArg(yazap.Arg.singleValueOption("vendor-insts-output", null, null));
    var vendor_prefix_arg = yazap.Arg.singleValueOption("vendor-operand-kind-prefix", null, null);
    vendor_prefix_arg.setProperty(.allow_empty_value);
    try root_cmd.addArg(vendor_prefix_arg);

    const matches = try app.parseProcess();
    if (!matches.containsArgs()) {
        try app.displayHelp();
        return 1;
    }

    if (matches.containsArg("core-insts-output") != matches.containsArg("operand-kinds-output")) {
        std.log.err("--core-insts-output and --operand-kinds-output should be specified together.\n", .{});
        return 1;
    } else if (
        matches.containsArg("operand-kinds-output") and !(
            matches.containsArg("spirv-core-grammar") and
            matches.containsArg("extinst-debuginfo-grammar") and
            matches.containsArg("extinst-cldebuginfo100-grammar")
        )
    ) {
        std.log.err(
            "--operand-kinds-output requires --spirv-core-grammar and " ++
            "--extinst-debuginfo-grammar and --extinst-cldebuginfo100-grammar\n",
            .{},
        );
        return 1;
    } else if (matches.containsArg("glsl-insts-output") != matches.containsArg("extinst-glsl-grammar")) {
        std.log.err("--glsl-insts-output and --extinst-glsl-grammar should be specified together.\n", .{});
        return 1;
    } else if (matches.containsArg("opencl-insts-output") != matches.containsArg("extinst-opencl-grammar")) {
        std.log.err(
            "--opencl-insts-output and --extinst-opencl-grammar should be specified together.\n",
            .{},
        );
        return 1;
    } else if (matches.containsArg("vendor-insts-output") != matches.containsArg("extinst-vendor-grammar")) {
        std.log.err(
            "--vendor-insts-output and --extinst-vendor-grammar should be specified together.\n",
            .{},
        );
        return 1;
    } else if (!(
        matches.containsArg("core-insts-output") or
        matches.containsArg("glsl-insts-output") or
        matches.containsArg("opencl-insts-output") or
        matches.containsArg("vendor-insts-output") or
        matches.containsArg("extension-enum-output") or
        matches.containsArg("enum-string-mapping-output")
    )) {
        std.log.err("at least one output should be specified.\n", .{});
        return 1;
    }

    const language_mode_raw = matches.getSingleValue("output-language") orelse "c";
    const language_mode = std.meta.stringToEnum(
        LanguageMode,
        switch (language_mode_raw.len == 0) {
            true => "c++",
            else => language_mode_raw,
        },
    ).?;

    // the current working directory does not need to be closed manually like
    // other directories, process exit will clean it up.
    const cwd = std.fs.cwd();

    if (matches.getSingleValue("spirv-core-grammar")) |spirv_core_grammar_raw_path| {
        const spirv_core_grammar = try parse_grammar_json(
            allocator,
            cwd,
            spirv_core_grammar_raw_path,
        );
        defer spirv_core_grammar.deinit();

        if (matches.getSingleValue("extinst-debuginfo-grammar")) |extinst_debuginfo_grammar_raw_path| {
            const extinst_debuginfo_grammar = try parse_grammar_json(
                allocator,
                cwd,
                extinst_debuginfo_grammar_raw_path,
            );
            defer extinst_debuginfo_grammar.deinit();

            if (matches.getSingleValue("extinst-cldebuginfo100-grammar")) |extinst_cldebuginfo100_grammar_raw_path| {
                const extinst_cldebuginfo100_grammar = try parse_grammar_json(
                    allocator,
                    cwd,
                    extinst_cldebuginfo100_grammar_raw_path,
                );
                defer extinst_cldebuginfo100_grammar.deinit();

                var extensions_set = std.StringArrayHashMap(void).init(allocator);
                defer extensions_set.deinit();
                try collect_extensions(&extensions_set, spirv_core_grammar.value);
                try collect_extensions(&extensions_set, extinst_debuginfo_grammar.value);
                try collect_extensions(&extensions_set, extinst_cldebuginfo100_grammar.value);

                for (EXTENSIONS_FROM_SPIRV_REGISTRY_AND_NOT_FROM_GRAMMARS) |ext| {
                    if (extensions_set.contains(ext)) {
                        std.debug.panic("Extension {s} is already in a grammar file\n", .{ ext });
                    }

                    try extensions_set.put(ext, {});
                }
                try extensions_set.put("SPV_VALIDATOR_ignore_type_decl_unique", {});

                const AllExtensionsSortContext = struct {
                    exts: [][]const u8,

                    pub fn lessThan(ctx: @This(), a_index: usize, b_index: usize) bool {
                        return std.mem.lessThan(u8, ctx.exts[a_index], ctx.exts[b_index]);
                    }
                };
                extensions_set.unmanaged.sortUnstable(AllExtensionsSortContext{ .exts = extensions_set.keys() });

                if (matches.getSingleValue("core-insts-output")) |core_insts_output_raw_path| {
                    const operand_kinds_output_raw_path = matches.getSingleValue("operand-kinds-output").?;

                    {
                        const instructions = try spirv_core_grammar.value.dupe_instructions_with_prefix(allocator, null);
                        defer instructions.deinit();

                        var core_inst_output_buffer = std.ArrayList(u8).init(allocator);
                        defer core_inst_output_buffer.deinit();
                        try generate_instruction_table(
                            allocator,
                            &core_inst_output_buffer,
                            instructions.value,
                            language_mode,
                        );
                        try write_buffer_to_file(cwd, core_insts_output_raw_path, core_inst_output_buffer.items);
                    }

                    {
                        var operand_kind_output_buffer = std.ArrayList(u8).init(allocator);
                        defer operand_kind_output_buffer.deinit();

                        try generate_operand_kind_table(
                            allocator,
                            &operand_kind_output_buffer,
                            spirv_core_grammar.value.operand_kinds.?,
                            extinst_debuginfo_grammar.value.operand_kinds.?,
                            extinst_cldebuginfo100_grammar.value.operand_kinds.?,
                        );
                        try write_buffer_to_file(cwd, operand_kinds_output_raw_path, operand_kind_output_buffer.items);
                    }
                }

                if (matches.getSingleValue("extension-enum-output")) |extension_enum_output_raw_path| {
                    var output_buffer = std.ArrayList(u8).init(allocator);
                    defer output_buffer.deinit();

                    var first_iter_complete = false;
                    for (extensions_set.keys()) |ext| {
                        if (first_iter_complete) {
                            try output_buffer.appendSlice(",\n");
                        }
                        first_iter_complete = true;

                        try output_buffer.append('k');
                        try output_buffer.appendSlice(ext);
                    }

                    try write_buffer_to_file(cwd, extension_enum_output_raw_path, output_buffer.items);
                }

                if (matches.getSingleValue("enum-string-mapping-output")) |enum_string_mapping_output_raw_path| {
                    var output_buffer = std.ArrayList(u8).init(allocator);
                    defer output_buffer.deinit();

                    try generate_extension_to_string_mapping(&output_buffer, extensions_set.keys());
                    try output_buffer.appendSlice("\n\n\n");
                    try generate_string_to_extension_mapping(&output_buffer, extensions_set.keys());
                    try output_buffer.appendSlice("\n    \n\n");

                    var capabilities_set = std.AutoArrayHashMap(u32, []const u8).init(allocator);
                    defer capabilities_set.deinit();
                    try collect_capabilities(&capabilities_set, spirv_core_grammar.value);
                    try collect_capabilities(&capabilities_set, extinst_debuginfo_grammar.value);
                    try collect_capabilities(&capabilities_set, extinst_cldebuginfo100_grammar.value);

                    const cap_type_string = switch (language_mode) {
                        .c => "SpvCapability",
                        .@"c++" => "spv::Capability",
                    };
                    const cap_type_string_joiner = switch (language_mode) {
                        .c => "",
                        .@"c++" => "::",
                    };
                    {
                        var writer = output_buffer.writer();
                        try writer.print(
                            "const char* CapabilityToString({[captype]s} capability) {{\n" ++
                            "  switch (capability) {{\n",
                            .{ .captype = cap_type_string },
                        );

                        for (capabilities_set.values()) |cap| {
                            try writer.print(
                                "    case {[captype]s}{[capjoin]s}{[capability]s}:\n" ++
                                "      return \"{[capability]s}\";\n",
                                .{
                                    .captype = cap_type_string,
                                    .capjoin = cap_type_string_joiner,
                                    .capability = cap,
                                }
                            );
                        }

                        try writer.print(
                            "    case {[captype]s}{[capjoin]s}Max:\n" ++
                            "      assert(0 && \"Attempting to convert {[captype]s}{[capjoin]s}Max to string\");\n" ++
                            "      return \"\";\n" ++
                            "  }}\n\n  return \"\";\n}}",
                            .{
                                .captype = cap_type_string,
                                .capjoin = cap_type_string_joiner,
                            },
                        );
                    }

                    try write_buffer_to_file(cwd, enum_string_mapping_output_raw_path, output_buffer.items);
                }
            }
        }
    }

    if (matches.getSingleValue("extinst-glsl-grammar")) |extinst_glsl_grammar_raw_path| {
        const glsl_insts_output_raw_path = matches.getSingleValue("glsl-insts-output").?;
        const grammer_set_name: []const u8 = "glsl";

        const instructions = blk: {
            const grammar = try parse_grammar_json(allocator, cwd, extinst_glsl_grammar_raw_path);
            defer grammar.deinit();

            break :blk try grammar.value.dupe_instructions_with_prefix(allocator, grammer_set_name);
        };
        defer instructions.deinit();

        var output_buffer = std.ArrayList(u8).init(allocator);
        defer output_buffer.deinit();

        try generate_extended_instruction_table(
            allocator,
            &output_buffer,
            instructions.value,
            grammer_set_name,
            language_mode,
        );

        try write_buffer_to_file(cwd, glsl_insts_output_raw_path, output_buffer.items);
    }

    if (matches.getSingleValue("extinst-opencl-grammar")) |extinst_opencl_grammar_raw_path| {
        const opencl_insts_output_raw_path = matches.getSingleValue("opencl-insts-output").?;
        const grammer_set_name: []const u8 = "opencl";

        const instructions = blk: {
            const grammar = try parse_grammar_json(allocator, cwd, extinst_opencl_grammar_raw_path);
            defer grammar.deinit();

            break :blk try grammar.value.dupe_instructions_with_prefix(allocator, grammer_set_name);
        };
        defer instructions.deinit();

        var output_buffer = std.ArrayList(u8).init(allocator);
        defer output_buffer.deinit();

        try generate_extended_instruction_table(
            allocator,
            &output_buffer,
            instructions.value,
            grammer_set_name,
            language_mode,
        );

        try write_buffer_to_file(cwd, opencl_insts_output_raw_path, output_buffer.items);
    }

    if (matches.getSingleValue("extinst-vendor-grammar")) |extinst_vendor_grammar_raw_path| {
        const vendor_insts_output_raw_path = matches.getSingleValue("vendor-insts-output").?;
        const vendor_operand_kind_prefix = matches.getSingleValue("vendor-operand-kind-prefix") orelse "";
        const grammer_set_name: []const u8 = try get_grammar_set_name(allocator, extinst_vendor_grammar_raw_path);
        defer allocator.free(grammer_set_name);

        const instructions = blk: {
            const grammar = try parse_grammar_json(allocator, cwd, extinst_vendor_grammar_raw_path);
            defer grammar.deinit();

            break :blk try grammar.value.dupe_instructions_with_prefix(allocator, vendor_operand_kind_prefix);
        };
        defer instructions.deinit();

        var output_buffer = std.ArrayList(u8).init(allocator);
        defer output_buffer.deinit();

        try generate_extended_instruction_table(
            allocator,
            &output_buffer,
            instructions.value,
            grammer_set_name,
            language_mode,
        );

        try write_buffer_to_file(cwd, vendor_insts_output_raw_path, output_buffer.items);
    }

    return 0;
}
